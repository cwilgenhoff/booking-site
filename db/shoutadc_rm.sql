-- phpMyAdmin SQL Dump
-- version 3.4.10.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 20-04-2012 a las 00:43:23
-- Versión del servidor: 5.0.95
-- Versión de PHP: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `shoutadc_rm`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `common`
--

CREATE TABLE IF NOT EXISTS `common` (
  `key` varchar(30) NOT NULL,
  `language` varchar(4) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY  (`key`,`language`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `common`
--

INSERT INTO `common` (`key`, `language`, `value`) VALUES
('body-info', 'en', '<h2><strong>Introduction</strong></h2>\r\n<p><span>Marbella Patio Restaurant is located in the centre of Marbella Old Town and has all the charm and authenticity that you are looking for if Spanish cuisine is what you are hankering after for lunch or dinner today!</span></p>\r\n<p><span>It''s by no means your average tapas bar or Spanish venta though, it''s really a very special place and offers a full a la carte menu with the option for a delicious tasting menu too. Their tapas are inspired and we were really impressed with the quality of ingredients and the attention to detail that has been paid to the decor and the presentation of their food.</span></p>\r\n<h3><strong><br /></strong></h3>\r\n<h2><strong>Price a la Carte&nbsp;<span>(menu)</span></strong></h2>\r\n<h4><strong><em><span>Appetizers + dish + dessert =</span>&nbsp;</em></strong><span>50&euro; / pp</span></h4>\r\n<h4><strong><br /></strong></h4>\r\n<h4><span><strong>Directions</strong></span></h4>\r\n<p>B<em>us<br /></em>Puerto Ban&uacute;s Bus, Estepona Bus &amp; Fuengirola Bus</p>\r\n<p><em>Parking</em><br />Old Town Marbella Parking<br /><br /><em>More indications</em><br />Near the Town Hall, Next to Marbella Castle</p>\r\n<p><strong><img title="Marbella" src="gallery/image/Restaurantes-Marbella(1).png" alt="Marbella" width="295" height="121" /><br /></strong></p>\r\n<h3>&nbsp;</h3>\r\n<h3>Payment Options</h3>\r\n<p>Credit Cards, Visa, Mastercard</p>\r\n<h3>&nbsp;</h3>\r\n<h3>Services</h3>\r\n<p>Air Conditionned<br />English Spoken</p>\r\n<h3>&nbsp;</h3>\r\n<h3>Groups</h3>\r\n<p>Private Party : Yes<br />Maximum Size Group : 200</p>\r\n<h3>&nbsp;</h3>\r\n<h3>More Information</h3>\r\n<p>Opening Hours<br />Lunch: from 13:00 to 16:00<br />Dinner: from 20:00 to 24:00<br /><br />Closing Days<br />Christmas Eve and New Year (dinners)</p>\r\n<h3><strong><br /></strong></h3>'),
('body-info', 'es', '<h2><strong>Presentaci&oacute;n</strong></h2>\r\n<p>&iexcl;Explora Andaluc&iacute;a a trav&eacute;s de los intensos sabores de su suculenta cocina!</p>\r\n<p>&iexcl;Visita el Restaurante Marbella Patio!</p>\r\n<p>En &eacute;l podr&aacute;s descubrir los diferentes matices de esta amplia tradici&oacute;n gastron&oacute;mica, ya que saborear&aacute;s tanto platos de toda la vida como recetas&nbsp; m&aacute;s creativas, como, el Lomo de Esturi&oacute;n al Horno con Verduras a la Plancha y Vinagreta de Manzana.</p>\r\n<p>Ubicado en pleno casco antiguo de Marbella, este restaurante es un&nbsp; antiguo palacete del siglo XVI que te encantar&aacute; por su preciosa y al mismo tiempo, sencilla decoraci&oacute;n.<br /><br /></p>\r\n<h2><strong>Precio&nbsp;<span>(Carta y men&uacute;s)</span></strong></h2>\r\n<h4><span><strong><em>Precio a la Carta (entrante + plato+ postre) =&nbsp;</em></strong>50&euro;</span></h4>\r\n<h4><strong><br /></strong></h4>\r\n<h4><span><strong>&iquest;C&oacute;mo llegar?</strong></span></h4>\r\n<p><em>Transporte p&uacute;blico</em><br />Puerto Ban&uacute;s Bus, Estepona Bus &amp; Fuengirola Bus<br /><br /><em>Estacionamiento</em><br />Estacionamiento del Old Town Marbella<br /><br /><em>Otras indicaciones</em><br />Cerca del Ayuntamiento, de lado al castillo Marbella</p>\r\n<p><strong><img title="how-to-get" src="views/img/qr.png" alt="how-to-get" width="295" height="121" /></strong></p>\r\n<h3>&nbsp;</h3>\r\n<h3>Formas de pago</h3>\r\n<p>Tarjeta de cr&eacute;dito, Tarjeta Visa, Tarjeta Mastercard</p>\r\n<h3>&nbsp;</h3>\r\n<h3>Servicios</h3>\r\n<p>Aire acondicionado<br />Se habla ingl&eacute;s</p>\r\n<h3>&nbsp;</h3>\r\n<h3>Grupos</h3>\r\n<p>Restaurante privatizable : S&iacute;<br />M&aacute;ximo de personas para grupos : 200</p>\r\n<h3>&nbsp;</h3>\r\n<h3>M&aacute;s informaci&oacute;n</h3>\r\n<p>Horario de apertura<br />Comida: De 13:00 a 16:00<br />Cena: De 16:30 a 24:00<br /><br />D&iacute;as de cierre<br />Nochebuena y A&ntilde;o Nuevo (cenas)</p>\r\n<p>&nbsp;</p>'),
('body-menue', 'en', '<h2><strong>El Chef recomienda</strong></h2>\r\n<p><span><em>Tray with</em><em> </em><em>5 Creative</em><em> </em><em>Tapas</em><em>&nbsp;&nbsp;&nbsp; </em><em>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;8,75&euro;</em></span></p>\r\n<p><em><br />Zucchini</em><em> </em><em>and</em><em> </em><em>Foie</em><em> </em><em>Timbal</em><em>&nbsp; with</em><em> PX</em><em> </em><em>Vinegar&nbsp; </em><em>&nbsp; &nbsp; &nbsp; &nbsp;8,85&euro;</em></p>\r\n<p><span>&nbsp;</span></p>\r\n<p><em>Ajo Blanco Malague&ntilde;o</em><em>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;4,90&euro; </em>&nbsp;</p>\r\n<p><span>&nbsp;</span></p>\r\n<p><em>Rice, Srhimps, Flat-Mushrooms and Squids &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</em>10,50&euro;</p>\r\n<p><span>&nbsp;</span></p>\r\n<p><em>Paella&nbsp; Marbella Patio</em><em>&nbsp;&nbsp;</em><em>Rice, Fish and <br />Shell-Fish&nbsp; x 1 pp. &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</em>13,75&euro;</p>\r\n<p><span>&nbsp;</span></p>\r\n<p><em>Baked Sturgeon </em><em>&nbsp;&nbsp;Over Fresh Vegetables<br />Grilled&nbsp;</em><em>With Apple, Tomato and Zucchini <br />Vinaigrette</em><em>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</em>14,50&euro;</p>\r\n<p><span>&nbsp;</span></p>\r\n<p><span><em>Sole Filets Marbella Patio&nbsp;</em><em>With Champagne <br />Sauce and&nbsp; Srhirmps </em>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 15,50&euro;<em></em></span></p>\r\n<p><span>&nbsp;</span></p>\r\n<p><em>Fish in Salt-Cooked<br /></em><em>(Depending</em><em> </em><em>the</em><em> </em><em>Market</em><em> </em><em>and</em><em> </em><em>Weight)</em></p>\r\n<p><span>&nbsp;<br /></span><em>Chicken in Red Curry&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</em>13,75&euro;<em> </em><em>&nbsp;&nbsp;&nbsp;</em>&nbsp;&nbsp;<em>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</em></p>\r\n<p><span>&nbsp;</span></p>\r\n<p><em>Ox-Tail Stewed</em><em>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</em>14,50&euro;</p>\r\n<p><span>&nbsp;</span></p>\r\n<p><em>Suckling Pig a la Segoviana &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</em>19,75&euro;</p>\r\n<p><span>&nbsp;</span></p>\r\n<p><span><strong><br /></strong></span></p>\r\n<h2><strong>Desserts</strong></h2>\r\n<p><strong><br /></strong></p>\r\n<p><span><em>Apple-Pie Home-Made &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</em>4,75&euro;<em></em></span></p>\r\n<p><span>&nbsp;</span></p>\r\n<p><span><em>Cheese-Cake with Fruits of the Forest &nbsp; &nbsp; &nbsp;</em>5,75&euro;<em>&nbsp; </em><em></em></span></p>\r\n<p><span>&nbsp;</span></p>\r\n<p><span><em>Cr&ecirc;pes &ldquo;Marbella Patio&rdquo; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</em>5,75&euro;</span><em> </em><strong></strong></p>\r\n<h4><em><br /></em></h4>'),
('body-menue', 'es', '<h2>El Chef recomienda</h2>\r\n<p><span>Bandeja de 5 Tapas Creativas</span></p>\r\n<p><span>8,75&euro;<br />&nbsp;</span></p>\r\n<p>Timbal de Calabacin</p>\r\n<p><span>&nbsp;8,85&euro;</span></p>\r\n<p><span>&nbsp;</span></p>\r\n<p><span>Ajo Blanco Malague&ntilde;o</span></p>\r\n<p><span>4,90&euro; &nbsp;</span></p>\r\n<p><span>&nbsp;</span></p>\r\n<p><span>Arroz Meloso</span></p>\r\n<p><span>10,50&euro;</span></p>\r\n<p><span>&nbsp;</span></p>\r\n<p><span>Paella Marbella Patio</span></p>\r\n<p><span>13,75&euro;</span></p>\r\n<p><span>&nbsp;</span></p>\r\n<p><span>Lomo de Esturi&oacute;n</span></p>\r\n<p><span>14,50&euro;</span></p>\r\n<p><span>&nbsp;</span></p>\r\n<p><span>Filetes de Lenguado Marbella Patio</span></p>\r\n<p><span>15,50&euro;</span></p>\r\n<p><span>&nbsp;</span></p>\r\n<p><span>Pescado a la Sal</span></p>\r\n<p><span>&nbsp;</span></p>\r\n<p><span>Pechuga de Pollo al Curry Rojo</span></p>\r\n<p><span>13,75&euro; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></p>\r\n<p><span>&nbsp;</span></p>\r\n<p><span>Rabo de Toro</span></p>\r\n<p><span>14,50&euro;</span></p>\r\n<p><span>&nbsp;</span></p>\r\n<p><span>Cochinillo Asado Tradicional</span></p>\r\n<p><span>19,75&euro;</span></p>\r\n<p><span>&nbsp;</span></p>\r\n<h2>Postres Caseros</h2>\r\n<p><strong><br /></strong></p>\r\n<p><span>Tarta de Queso con frutas del bosque<br />4,75&euro;</span></p>\r\n<p><span>&nbsp;</span></p>\r\n<p><span>Tarta de Queso con Frutas&nbsp; del Bosque</span></p>\r\n<p><span>&nbsp;5,75&euro;&nbsp;</span></p>\r\n<p><span>&nbsp;</span></p>\r\n<p><span>Crepes&nbsp; Marbella Patio</span></p>\r\n<p><span>&nbsp;5,75&euro;</span> <strong></strong></p>\r\n<h4><em><br /></em></h4>'),
('email-client-confirmation', 'en', ''),
('email-client-confirmation', 'es', ''),
('email-recommendations', 'en', ''),
('email-recommendations', 'es', ''),
('email-restaurant-confirmation', 'en', ''),
('email-restaurant-confirmation', 'es', ''),
('img-restaurant', 'en', ''),
('img-restaurant', 'es', ''),
('max-reservation-restaurant', 'en', ''),
('max-reservation-restaurant', 'es', ''),
('name-restaurant', 'en', ''),
('name-restaurant', 'es', ''),
('sms-confirmation', 'en', ''),
('sms-confirmation', 'es', ''),
('url-restaurant', 'en', 'http://www.marbellapatio.com'),
('url-restaurant', 'es', 'http://www.marbellapatio.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reservations`
--

CREATE TABLE IF NOT EXISTS `reservations` (
  `id` int(11) NOT NULL auto_increment,
  `date_booked` date NOT NULL,
  `time_booked` time NOT NULL,
  `people_booked` int(11) NOT NULL,
  `username` varchar(32) NOT NULL,
  `date_logged` datetime NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `username` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL,
  `mod_rights` tinyint(1) NOT NULL default '0' COMMENT 'Intended for administration rights.',
  `name` varchar(32) NOT NULL,
  `surname` varchar(32) NOT NULL,
  `email` varchar(32) NOT NULL,
  `cp` int(8) NOT NULL,
  `phone` varchar(32) NOT NULL,
  `hybridauth_provider_name` varchar(20) NOT NULL default '' COMMENT 'provider name if the user is signed up with hybridauth',
  `hybridauth_provider_uid` varchar(255) NOT NULL default '' COMMENT 'The Unique user ID on the used provider. if the user is signed up with hybridauth',
  PRIMARY KEY  (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`username`, `password`, `mod_rights`, `name`, `surname`, `email`, `cp`, `phone`, `hybridauth_provider_name`, `hybridauth_provider_uid`) VALUES
('admin', '1ce6b41c3ae97eae0e86cc9a7707a252', 1, 'Administrator', '', 'info@marbellaresto.com', 0, '', '''''', ''''''),
('cinternete', 'c9c063abbbb22edf37ab449ea0dca848', 0, 'C', 'InternetE', 'cristian.wil88@gmail.com', 7000, '0', '', ''),
('cwilgenhoff', 'd7590ece7872fffa89bb58f60921c913', 0, 'Cristian', 'Wilgenhoff', 'cpwilgenhoff@gmail.com', 7000, '0054298315642518', '', ''),
('ggomez rufino', 'd080a94f0995f3c27b8e8f46ca6a6598', 0, 'Gonzalo', 'Gomez Rufino', 'gonzalo@shoutad.com', 29649, '669171949', 'facebook', '737054444'),
('ginternet explorer', '751547768268e3e465ac5c0086470bc8', 0, 'gonzalo EXPLORER', 'internet EXPLORER', 'gonzalo@shoutad.com', 23423, '0000000', '', ''),
('glast name', 'fec37a210f586e8c9320919d3b80a6e4', 0, 'Gonzus', 'Last Name', 'gonzalo@shoutad.com', 29603, '0000000', '', ''),
('pgomez', '8d257e231f72572035b204cee2e055cc', 0, 'prueba5', 'gomez', 'info@shoutad.com', 29603, '0000000', '', ''),
('pgomez rufino', 'b7411229b4dc6346320d703fce043cc2', 0, 'Prueba6', 'gomez rufino', 'g.on.za@hotmail.com', 29649, '0000000', '', ''),
('psrusansma', '4f3669cfb87f8699af39cccfc3ef6be5', 0, 'prueba2', 'srusansma', 'info@shoutad.com', 29603, '0000000', '', ''),
('psurname', 'eed57faaa0aac06649a41779d9d5de87', 0, 'prueba2', 'surname', 'info@shoutad.com', 29603, '0000000', '', ''),
('psurneams', '555b7efc9a5cc22a30eae4873914c46a', 0, 'prueba1', 'surneams', 'info@shoutad.com', 29603, '0000000', '', ''),
('sadsada', '8f1620a2e4fb2d37bcdd433ea909e09a', 0, 'sdads', 'adsada', 'dasda@prueba.com', 3434, '00000', '', '');

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `reservations`
--
ALTER TABLE `reservations`
  ADD CONSTRAINT `reservations_ibfk_1` FOREIGN KEY (`username`) REFERENCES `users` (`username`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
