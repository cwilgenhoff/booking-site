-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 20, 2012 at 06:17 AM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `booking-db`
--

-- --------------------------------------------------------

--
-- Table structure for table `common`
--

CREATE TABLE IF NOT EXISTS `common` (
  `key` varchar(30) NOT NULL,
  `language` varchar(4) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`key`,`language`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `common`
--

INSERT INTO `common` (`key`, `language`, `value`) VALUES
('body-info', 'en', '<h2><strong>Introduction</strong></h2>\r\n<p><span>Marbella Patio Restaurant is located in the centre of Marbella Old Town and has all the charm and authenticity that you are looking for if Spanish cuisine is what you are hankering after for lunch or dinner today!<br /></span><br />It''s by no means your average tapas bar or Spanish venta though, it''s really a very special place and offers a full a la carte menu with the option for a delicious tasting menu too. Their tapas are inspired and we were really impressed with the quality of ingredients and the attention to detail that has been paid to the decor and the presentation of their food.</p>\r\n<h3><strong><br /></strong></h3>\r\n<h2><strong>Price a la Carte&nbsp;<span>(menu)</span></strong></h2>\r\n<h4><strong><em><span>Appetizers + dish + dessert =</span>&nbsp;</em></strong><span>50&euro; / pp</span></h4>\r\n<h4><strong><br /></strong></h4>\r\n<h4><span><strong>Directions</strong></span></h4>\r\n<p>B<em>us<br /></em>Puerto Ban&uacute;s Bus, Estepona Bus &amp; Fuengirola Bus</p>\r\n<p><em>Parking</em><br />Old Town Marbella Parking<br /><br /><em>More indications</em><br />Near the Town Hall, Next to Marbella Castle</p>\r\n<p><strong><img title="Marbella" src="gallery/image/Restaurantes-Marbella(1).png" alt="Marbella" width="295" height="121" /><br /></strong></p>\r\n<h3>&nbsp;</h3>\r\n<h3>Payment Options</h3>\r\n<p>Credit Cards, Visa, Mastercard</p>\r\n<h3>&nbsp;</h3>\r\n<h3>Services</h3>\r\n<p>Air Conditionned<br />English Spoken</p>\r\n<h3>&nbsp;</h3>\r\n<h3>Groups</h3>\r\n<p>Private Party : Yes<br />Maximum Size Group : 200</p>\r\n<h3>&nbsp;</h3>\r\n<h3>More Information</h3>\r\n<p>Opening Hours<br />Lunch: from 13:00 to 16:00<br />Dinner: from 20:00 to 24:00<br /><br />Closing Days<br />Christmas Eve and New Year (dinners)</p>\r\n<h3><strong><br /></strong></h3>'),
('body-info', 'es', '<h2><strong>Presentaci&oacute;n</strong></h2>\r\n<p>&iexcl;Explora Andaluc&iacute;a a trav&eacute;s de los intensos sabores de su suculenta cocina!<br /><br />&iexcl;Visita el Restaurante Marbella Patio!<br /><br />En &eacute;l podr&aacute;s descubrir los diferentes matices de esta amplia tradici&oacute;n gastron&oacute;mica, ya que saborear&aacute;s tanto platos de toda la vida como recetas&nbsp; m&aacute;s creativas, como, el Lomo de Esturi&oacute;n al Horno con Verduras a la Plancha y Vinagreta de Manzana.<br /><br />Ubicado en pleno casco antiguo de Marbella, este restaurante es un&nbsp; antiguo palacete del siglo XVI que te encantar&aacute; por su preciosa y al mismo tiempo, sencilla decoraci&oacute;n.<br /><br />&nbsp;</p>\r\n<h2><strong>Precio&nbsp;<span>(Carta y men&uacute;s)</span></strong></h2>\r\n<h4><span><strong><em>Precio a la Carta (entrante + plato+ postre) =&nbsp;</em></strong>50&euro;</span></h4>\r\n<h4><strong><br /></strong></h4>\r\n<h4><span><strong>&iquest;C&oacute;mo llegar?</strong></span></h4>\r\n<p><em>Transporte p&uacute;blico</em><br />Puerto Ban&uacute;s Bus, Estepona Bus &amp; Fuengirola Bus<br /><br /><em>Estacionamiento</em><br />Estacionamiento del Old Town Marbella<br /><br /><em>Otras indicaciones</em><br />Cerca del Ayuntamiento, de lado al castillo Marbella</p>\r\n<p><strong><img title="how-to-get" src="views/img/qr.png" alt="how-to-get" width="295" height="121" /></strong></p>\r\n<h3>&nbsp;</h3>\r\n<h3>Formas de pago</h3>\r\n<p>Tarjeta de cr&eacute;dito, Tarjeta Visa, Tarjeta Mastercard</p>\r\n<h3>&nbsp;</h3>\r\n<h3>Servicios</h3>\r\n<p>Aire acondicionado<br />Se habla ingl&eacute;s</p>\r\n<h3>&nbsp;</h3>\r\n<h3>Grupos</h3>\r\n<p>Restaurante privatizable : S&iacute;<br />M&aacute;ximo de personas para grupos : 200</p>\r\n<h3>&nbsp;</h3>\r\n<h3>M&aacute;s informaci&oacute;n</h3>\r\n<p>Horario de apertura<br />Comida: De 13:00 a 16:00<br />Cena: De 16:30 a 24:00<br /><br />D&iacute;as de cierre<br />Nochebuena y A&ntilde;o Nuevo (cenas)</p>\r\n<p>&nbsp;</p>'),
('body-menue', 'en', '<h2>Chef recommendations</h2>\r\n<table border="0">\r\n<tbody>\r\n<tr style="height: 35px;" align="left">\r\n<td>Tray with 5 Creative Tapas &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</td>\r\n<td>8,75&euro;</td>\r\n</tr>\r\n<tr style="height: 35px;" align="left">\r\n<td>Zucchini and Foie Timbal &nbsp;with PX Vinegar</td>\r\n<td>8,85&euro;</td>\r\n</tr>\r\n<tr style="height: 35px;" align="left">\r\n<td>Ajo Blanco Malague&ntilde;o</td>\r\n<td>4,90&euro;</td>\r\n</tr>\r\n<tr style="height: 35px;" align="left">\r\n<td>Rice, Srhimps, Flat-Mushrooms and Squids</td>\r\n<td>10,50&euro;</td>\r\n</tr>\r\n<tr style="height: 35px;" align="left">\r\n<td>\r\n<p>Paella Marbella Patio Rice, Fish <br />and Shell-Fish x 1 pp.</p>\r\n</td>\r\n<td>13,75&euro;</td>\r\n</tr>\r\n<tr style="height: 35px;" align="left">\r\n<td>\r\n<p>Baked Sturgeon Over Fresh Vegetables<br />Grilled With Apple, Tomato and Zucchini <br />Vinaigrette</p>\r\n</td>\r\n<td>14,50&euro;</td>\r\n</tr>\r\n<tr style="height: 35px;" align="left">\r\n<td>\r\n<p>Sole Filets Marbella Patio With Champagne <br />Sauce and Srhirmps</p>\r\n</td>\r\n<td>15,50&euro;</td>\r\n</tr>\r\n<tr style="height: 35px;" align="left">\r\n<td>\r\n<p>Fish in Salt-Cooked<br />(Depending the Market and Weight)</p>\r\n</td>\r\n<td>&nbsp;</td>\r\n</tr>\r\n<tr style="height: 35px;" align="left">\r\n<td>Chicken in Red Curry</td>\r\n<td>13,75&euro;</td>\r\n</tr>\r\n<tr style="height: 35px;" align="left">\r\n<td>Ox-Tail Stewed</td>\r\n<td>14,50&euro;</td>\r\n</tr>\r\n<tr style="height: 35px;" align="left">\r\n<td>Suckling Pig a la Segoviana</td>\r\n<td>19,75&euro;</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<h2><br />Desserts</h2>\r\n<table border="0">\r\n<tbody>\r\n<tr style="height: 35px;" align="left">\r\n<td>Apple-Pie Home-Made&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</td>\r\n<td>4,75&euro;</td>\r\n</tr>\r\n<tr style="height: 35px;" align="left">\r\n<td>Cheese-Cake with Fruits of the Forest</td>\r\n<td>5,75&euro;</td>\r\n</tr>\r\n<tr style="height: 35px;" align="left">\r\n<td>Cr&ecirc;pes &ldquo;Marbella Patio&rdquo;</td>\r\n<td>5,75&euro;</td>\r\n</tr>\r\n</tbody>\r\n</table>'),
('body-menue', 'es', '<h2>El Chef recomienda</h2>\r\n<table border="0">\r\n<tbody>\r\n<tr style="height: 35px;" align="left">\r\n<td>Bandeja de 5 Tapas Creativas &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</td>\r\n<td>8,75&euro;</td>\r\n</tr>\r\n<tr style="height: 35px;" align="left">\r\n<td>Timbal de Calabacin</td>\r\n<td>8,85&euro;</td>\r\n</tr>\r\n<tr style="height: 35px;" align="left">\r\n<td>Ajo Blanco Malague&ntilde;o</td>\r\n<td>4,90&euro;</td>\r\n</tr>\r\n<tr style="height: 35px;" align="left">\r\n<td>Arroz Meloso</td>\r\n<td>10,50&euro;</td>\r\n</tr>\r\n<tr style="height: 35px;" align="left">\r\n<td>Paella Marbella Patio</td>\r\n<td>13,75&euro;</td>\r\n</tr>\r\n<tr style="height: 35px;" align="left">\r\n<td>Lomo de Esturi&oacute;n</td>\r\n<td>14,50&euro;</td>\r\n</tr>\r\n<tr style="height: 35px;" align="left">\r\n<td>Filetes de Lenguado Marbella Patio</td>\r\n<td>15,50&euro;</td>\r\n</tr>\r\n<tr style="height: 35px;" align="left">\r\n<td>Pescado a la Sal<br />(Depende del mercado y el paso)</td>\r\n<td>&nbsp;</td>\r\n</tr>\r\n<tr style="height: 35px;" align="left">\r\n<td>Pechuga de Pollo al Curry Rojo</td>\r\n<td>13,75&euro;</td>\r\n</tr>\r\n<tr style="height: 35px;" align="left">\r\n<td>Rabo de Toro</td>\r\n<td>14,50&euro;</td>\r\n</tr>\r\n<tr style="height: 35px;" align="left">\r\n<td>Cochinillo Asado Tradicional</td>\r\n<td>19,75&euro;</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<h2><br />Postres Caseros</h2>\r\n<table border="0">\r\n<tbody>\r\n<tr style="height: 35px;" align="left">\r\n<td>Tarta de Queso con Frutas del Bosque &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</td>\r\n<td>4,75&euro;</td>\r\n</tr>\r\n<tr style="height: 35px;" align="left">\r\n<td>Tarta de Queso con Frutas del Bosque</td>\r\n<td>5,75&euro;</td>\r\n</tr>\r\n<tr style="height: 35px;" align="left">\r\n<td>Crepes &nbsp;Marbella Patio</td>\r\n<td>5,75&euro;</td>\r\n</tr>\r\n</tbody>\r\n</table>'),
('body-offer', 'en', ''),
('body-offer', 'es', ''),
('email-client-confirmation', 'en', '<p>Dear {$username},<br /><br /></p>\r\n<p><br />Your reservation request for Marbella Patio Restaurant was submitted through Shout! APP reserve a table on {$date}.<br /><br /></p>\r\n<p><br /><strong>Please note your reservation is confirmed !</strong></p>\r\n<p><br /><br /><em>Restaurant Marbella Patio</em><br /><em>+34 952 775 429.</em><br /><em>Calle Virgen de los Dolores, 4, 29601 Marbella</em></p>\r\n<p><em>Google Maps: <a href="http://goo.gl/y5jsr" target="_blank">http://goo.gl/y5jsr</a></em></p>\r\n<p>&nbsp;</p>\r\n<p><br /><span style="font-size: x-small;">Kind Regards,</span><br /><span style="font-size: x-small;"><a href="http://www.marbellapatio.com" target="_blank"><br />www.marbellapatio.com</a>&nbsp;</span><br /><span style="font-size: x-small;">by&nbsp;<a href="http://shoutad.com" target="_blank">SHOUT! marketing</a></span></p>'),
('email-client-confirmation', 'es', ''),
('email-recommendations', 'en', ''),
('email-recommendations', 'es', ''),
('email-restaurant-confirmation', 'en', ''),
('email-restaurant-confirmation', 'es', ''),
('img-restaurant', 'en', ''),
('img-restaurant', 'es', ''),
('max-reservation-restaurant', 'en', ''),
('max-reservation-restaurant', 'es', ''),
('name-restaurant', 'en', 'Marbella Patio'),
('name-restaurant', 'es', ''),
('sms-confirmation', 'en', ''),
('sms-confirmation', 'es', '');

-- --------------------------------------------------------

--
-- Table structure for table `reservations`
--

CREATE TABLE IF NOT EXISTS `reservations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_booked` date NOT NULL,
  `time_booked` time NOT NULL,
  `people_booked` int(11) NOT NULL,
  `username` varchar(32) NOT NULL,
  `date_logged` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `username` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL,
  `mod_rights` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Intended for administration rights.',
  `name` varchar(32) NOT NULL,
  `surname` varchar(32) NOT NULL,
  `email` varchar(32) NOT NULL,
  `cp` int(8) NOT NULL,
  `phone` varchar(32) NOT NULL,
  `hybridauth_provider_name` varchar(20) NOT NULL DEFAULT '' COMMENT 'provider name if the user is signed up with hybridauth',
  `hybridauth_provider_uid` varchar(255) NOT NULL DEFAULT '' COMMENT 'The Unique user ID on the used provider. if the user is signed up with hybridauth',
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`username`, `password`, `mod_rights`, `name`, `surname`, `email`, `cp`, `phone`, `hybridauth_provider_name`, `hybridauth_provider_uid`) VALUES
('admin', '1ce6b41c3ae97eae0e86cc9a7707a252', 1, 'Administrator', '', 'info@marbellaresto.com', 0, '', '''''', '''''');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `reservations`
--
ALTER TABLE `reservations`
  ADD CONSTRAINT `reservations_ibfk_1` FOREIGN KEY (`username`) REFERENCES `users` (`username`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
