<?php

include("models/mauthorizer.php");

session_start();

$Auth = MAuthorizer::GetInstance();
$Auth->LogOut();

PageController::Redirect($_GET["return"]);