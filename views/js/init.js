var current_language = document.getElementsByTagName('html')[0].getAttribute('lang');

function openKCFinder(field_name, url, type, win) {
    tinyMCE.activeEditor.windowManager.open({
        file: 'includes/kcfinder/browse.php?opener=tinymce&type=' + type,
        title: 'Galeria',
        width: 700,
        height: 500,
        resizable: "yes",
        inline: true,
        close_previous: "no",
        popup_css: false
    }, {
        window: win,
        input: field_name
    });
    return false;
}

function Init_Editors() {
	tinyMCE.init({
		mode : "textareas",
		theme:"advanced",
		language : current_language,
		content_css : "./views/css/style.css",
        plugins:"pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave",
		theme_advanced_buttons1:"save,newdocument,|,bold,italic,underline,strikethrough,forecolor,backcolor,image,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2:"cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,cleanup,help,|,insertdate,inserttime,preview",		
		theme_advanced_buttons3:"tablecontrols",
		theme_advanced_toolbar_location:"top",
		theme_advanced_toolbar_align:"left",
		theme_advanced_statusbar_location:"bottom",
		theme_advanced_resizing:true,		
		forced_root_block : false,
	    force_br_newlines : true,
	    force_p_newlines : false,
        table_styles : "Header 1=header1;Header 2=header2;Header 3=header3",
        table_cell_styles : "Header 1=header1;Header 2=header2;Header 3=header3;Table Cell=tableCel1",
        table_row_styles : "Header 1=header1;Header 2=header2;Header 3=header3;Table Row=tableRow1",
        table_cell_limit : 100,
        table_row_limit : 50,
        table_col_limit : 10,
        table_inline_editing : true,
		file_browser_callback: "openKCFinder"
	});
}

function GetCurrentHash()
{
   if(window.location.hash) 
   {
	  var hash = window.location.hash.substring(1); //Puts hash in variable, and removes the # character
	  return hash;
   }
   return "";
}

function MakeVisibleOnlySection(id)
{
	$('div.section').not('#'+id).each(function () {
		$($(this).addClass("hidden")).hide();
	});
	
	$($('#'+id).removeClass("hidden")).show();						
}

function LoadGalleryFrame() {
    $('#gallery-wrapper').slideDown('fast');
    $('#gallery-wrapper').attr('src', $('#gallery-wrapper').attr('link'));    
}

function BindActions()
{
	if (GetCurrentHash() != '')
	{
		MakeVisibleOnlySection(GetCurrentHash() + '-section');
		if (GetCurrentHash()== 'gallery')
		{
			LoadGalleryFrame();
		}
		if (GetCurrentHash()== 'info' || GetCurrentHash()== 'offer' || GetCurrentHash()== 'menue' || GetCurrentHash()== 'configs')
		{
			Init_Editors();
		}
	}
	if (GetCurrentHash() == 'users')		
	{
		$("#content").load("users.php #content > *");
	}

	$("#menu-left").on("click", "#reservations", function(event){
		
		if (GetCurrentHash() == 'users')
		{
			$("#content").load("admin.php #content > *", function(event) {
				MakeVisibleOnlySection("reservations-section");				
			});
			return;
		}

		MakeVisibleOnlySection("reservations-section");								
	});
	$("#menu-left").on("click", "#info", function(event){
		if (GetCurrentHash() == 'users')
		{
			$("#content").load("admin.php #content > *", function(event) {
				MakeVisibleOnlySection("info-section");	
				Init_Editors();
			});
			return;
		}

		MakeVisibleOnlySection("info-section");
		Init_Editors();
	});
	$("#menu-left").on("click", "#gallery", function(event){
		if (GetCurrentHash() == 'users')
		{
			$("#content").load("admin.php #content > *", function(event) {
				MakeVisibleOnlySection("gallery-section");			
				LoadGalleryFrame();
			});
			
			return;
		}

		MakeVisibleOnlySection("gallery-section");
		LoadGalleryFrame();		
	});
	$("#menu-left").on("click", "#menue", function(event){
		if (GetCurrentHash() == 'users')
		{
			$("#content").load("admin.php #content > *", function(event) {
				MakeVisibleOnlySection("menue-section");
				Init_Editors();
			});
			return;
		}
		
		MakeVisibleOnlySection("menue-section");
		Init_Editors();
	});
	$("#menu-left").on("click", "#offer", function(event){
		if (GetCurrentHash() == 'users')
		{
			$("#content").load("admin.php #content > *", function(event) {
					 MakeVisibleOnlySection("offer-section");
					 Init_Editors();
			});
			return;
		}

		MakeVisibleOnlySection("offer-section");
		Init_Editors();
	});
	$("#menu-left").on("click", "#configs", function(event){

		if (GetCurrentHash() == 'users')
		{
			$("#content").load("admin.php #content > *", function(event) { 
					MakeVisibleOnlySection("configs-section");
					Init_Editors();
			});
			return;
		}

		MakeVisibleOnlySection("configs-section");
		Init_Editors();
	});
	
	$("#menu-left").on("click", "#users", function(event){
		$("#content").load("users.php?language=" + current_language + " #content > *");
	});	
}