<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php echo $this->GetCurrentLanguage(); ?>" >
    <head>
        <title>Sitio de Administraci&oacute;n</title>
        <meta http-equiv = "Content-Type" content = "text/html; charset=utf-8" />
        <link href="./views/css/style.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
		<script type="text/javascript" src="views/js/tiny_mce/tiny_mce.js"></script>
		<script type="text/javascript" src="./views/js/jquery.qtip-1.0.0-rc3.min.js"></script>
        <script type="text/javascript" src="views/js/init.js"></script>		
		<script type="text/javascript">	
		
		function AddToolTip(id, _content)
		{
			$(id).qtip({
                content: _content,
                position: {
                	corner: {
                    	target: 'leftMiddle',
                        tooltip: 'rightMiddle'
                        }
                    },
                    style: { 
                    	name: 'cream',
                        tip: 'rightMiddle'
                    },
                    show: {
                    	when: {
                        	event: 'mouseover'
                            }
                        },
                        hide: {
                        	when: {
                            event: 'mouseout'
                        	}
                        }
             });
		}
		
		// Wait until the DOM has loaded before querying the document
		$(document).ready(function(){			
			BindActions();
			
			AddToolTip('#sms-confirmation', '<?php echo $this->GetText('wildcards-info'); ?>');
			AddToolTip('#email-client-confirmation-section', '<?php echo $this->GetText('wildcards-info'); ?>');
			AddToolTip('#email-restaurant-confirmation-section', '<?php echo $this->GetText('wildcards-info'); ?>')
		});
		</script>
    </head>
    <body class="greybg">
    	<div id="admin-panel">
	        <div id="header">
	        	<ul id="languages">
					<li id="lang-eng"><a href="?language=en" id="english" title="Switch to English">English</a></li>
					<li id="lang-spa"><a href="?language=es" id="spanish" title="Cambiar al Espa&ntilde;ol">Espa&ntilde;ol</a></li>			
				</ul>
	        </div>
	        <div id="center">
	        	<div id="menu-left">
	        		<?php $this->GetLeftMenu(); ?>
	        	</div>
	        	<div id="content">
		        	<div id="reservations-section" class="section">
			        	<h2><?php echo $this->GetText('admin-of'); ?> <?php echo $this->GetText('reservations'); ?></h2>
			        	<div>
				        	<table id="tablelistview">
			            		<thead>
	    						<tr>
	    							<th scope='col'><?php echo $this->GetText('date-reservation'); ?></th>
							    	<th scope='col'><?php echo $this->GetText('time-reservation'); ?></th>
							    	<th scope='col'><?php echo $this->GetText('people-reservation'); ?></th>						    	
							    	<th scope='col'><?php echo $this->GetText('name-simple'); ?></th>
							    	<th scope='col'><?php echo $this->GetText('surname-simple'); ?></th>
							    	<th scope='col'><?php echo $this->GetText('logged-date-reservation'); ?></th>
							    	<th scope='col'><?php echo $this->GetText('action'); ?></th>
						    	</tr>
	    						</thead>
	    						<tbody>
	    							<?php $this->GetReservationsAsTableBody(); ?>
	    						</tbody>		            		
			            	</table>		            	
			            </div>
		            </div>
					<div id="info-section" class="section hidden">
		            	<h2><?php echo $this->GetText('admin-of'); ?> <?php echo $this->GetText('info-n-sales'); ?></h2>
						<form action="admin.php?action=info" method="post">
							<div>
								<?php $this->GetAlerts(); ?>
								<p>
								<label for="body-info"><?php echo $this->GetText('content'); ?></label>
								<textarea rows="30" cols="20" name="body-info" id="body-info" class="input-content input" style="width: 100%; height: 15em"><?php echo $this->GetData('body-info'); ?></textarea>
								</p>
								
								<input type="submit" name="submit" class="button-primary submit"  value="<?php echo $this->GetText('save'); ?>"/>
							</div>
						</form>
		            </div>
		            <div id="offer-section" class="section hidden">
		            	<h2><?php echo $this->GetText('admin-of'); ?> <?php echo $this->GetText('offer'); ?></h2>
						<form action="admin.php?action=offer" method="post">
							<div>
								<?php $this->GetAlerts(); ?>
								<p>
								<label for="body-offer"><?php echo $this->GetText('content'); ?></label>
								<textarea rows="30" cols="20" name="body-offer" id="body-offer" class="input-content input" style="width: 100%; height: 15em"><?php echo $this->GetData('body-offer'); ?></textarea>
								</p>
								
								<input type="submit" name="submit" class="button-primary submit"  value="<?php echo $this->GetText('save'); ?>"/>
							</div>
						</form>
		            </div>
		            <div id="gallery-section" class="section hidden">
		            	<h2><?php echo $this->GetText('admin-of'); ?> <?php echo $this->GetText('gallery'); ?></h2>
						<iframe id="gallery-wrapper" src="" link="includes/kcfinder/browse.php?lang=es&amp;type=slideshow" style="width:100%; height:30em;"></iframe>												
		            </div>		            
		            <div id="menue-section" class="section hidden">
		            	<h2><?php echo $this->GetText('admin-of'); ?> <?php echo $this->GetText('menue'); ?></h2>
						<form action="admin.php?action=menue" method="post">
							<div>
								<?php $this->GetAlerts(); ?>
								<p>
								<label for="body-menue"><?php echo $this->GetText('content'); ?></label>
								<textarea rows="30" cols="20" name="body-menue" id="body-menue" class="input-content input" style="width: 100%; height: 15em" ><?php echo $this->GetData('body-menue'); ?></textarea>
								</p>
								
								<input type="submit" name="submit" class="button-primary submit"  value="<?php echo $this->GetText('save'); ?>"/>
							</div>
						</form>
					</div>		            
		            <div id="configs-section" class="section hidden">
		            	<h2><?php echo $this->GetText('configurations'); ?></h2>
						<form action="admin.php?action=configs" method="post">
							<div>
								<?php $this->GetAlerts(); ?>
								<p>
								<label for="name-restaurant"><?php echo $this->GetText('name-restaurant'); ?></label>
								<input type="text" name="name-restaurant" id="name-restaurant" class="input" value="<?php echo $this->GetData('name-restaurant'); ?>"/>
								</p>
								<p>
								<label for="max-reservation-restaurant"><?php echo $this->GetText('max-reservation-restaurant'); ?></label>
								<input type="text" name="max-reservation-restaurant" id="max-reservation-restaurant" class="input" value="<?php echo $this->GetData('max-reservation-restaurant'); ?>"/>
								</p>
								<p>
								<label for="sms-confirmation"><?php echo $this->GetText('sms-confirmation'); ?></label>
								<input type="text" name="sms-confirmation" id="sms-confirmation" maxlength="150" class="input" value="<?php echo $this->GetData('sms-confirmation'); ?>"/>
								</p>								
								<p id="email-client-confirmation-section">
								<label for="email-client-confirmation"><?php echo $this->GetText('email-client-confirmation'); ?></label>
								<textarea rows="20" cols="20" name="email-client-confirmation" id="email-client-confirmation" class="input-content input" style="width: 100%; height: 15em"><?php echo $this->GetData('email-client-confirmation'); ?></textarea>
								</p>
								<p id="email-restaurant-confirmation-section">
								<label for="email-restaurant-confirmation"><?php echo $this->GetText('email-restaurant-confirmation'); ?></label>
								<textarea rows="20" cols="20" name="email-restaurant-confirmation" id="email-restaurant-confirmation" class="input-content input" style="width: 100%; height: 15em"><?php echo $this->GetData('email-restaurant-confirmation'); ?></textarea>
								</p>
								<p>
								<label for="email-recommendations"><?php echo $this->GetText('email-recommendations'); ?></label>
								<textarea rows="20" cols="20" name="email-recommendations" id="email-recommendations" class="input-content input" style="width: 100%; height: 15em"><?php echo $this->GetData('email-recommendations'); ?></textarea>
								</p>								
								<input type="submit" name="submit" class="button-primary submit"  value="<?php echo $this->GetText('save'); ?>"/>
							</div>
						</form>
		            </div>
	            </div>
	            <div class="clear"></div>
	        </div>
        </div>
    </body> 
</html>