<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Log In</title>
        <meta http-equiv = "Content-Type" content = "text/html; charset=utf-8" />
        <link href="./views/css/style.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div id="login">
			<h1><a href="http://shoutad.com/" title="Powered by SHOUT! Marketing"> [ SHOUT! Marketing ]</a></h1>
            <form id="loginform" action="" method="post">
                <div>
                	<?php $this->GetAlerts(); ?>
                	<br />
					<p>
						<label for="username">Usuario: <span class="req">*</span></label>
						<input class="input" type="text" name="username" value=""/>						
					</p>					
                    
					<p>
						<label for="password">Contrase&ntilde;a: <span class="req">*</span></label>
						<input class="input" type="password" name="password" value=""/>						
					</p>
					
                    <p class="req">* campos requeridos</p>
                    
                    <input type="submit" name="submit" class="button-primary submit"  value="Entrar"/>
                </div>
            </form>						
        </div>
    </body> 
</html>

