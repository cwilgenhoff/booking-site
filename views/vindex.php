<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><?php echo $this->GetText('title'); ?></title>
    <style type="text/css">
        #galleria {
            width: 768px;
            height: 425px;
            color: #EFEFEF;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="./views/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="./views/css/datepicker.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="./views/js/lang/<?php echo $this->GetCurrentLanguage(); ?>.js"></script>
    <script type="text/javascript" src="./views/js/datepicker.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src="./views/js/jquery.columnizer.js"></script>
    <script type="text/javascript" src="./views/js/jquery.qtip-1.0.0-rc3.min.js"></script>
    <script type="text/javascript" src="./views/js/galleria/galleria-1.2.7.min.js"></script>
    <script type="text/javascript" src="./views/js/galleria/themes/classic/galleria.classic.min.js"></script>

    <?php $this->getReturnUrl(); ?>

    <script type="text/javascript">

        // Initialize Galleria
        Galleria.run('#galleria', { width:768, height:425, wait:true });

        function GetCurrentHash() {
            if (window.location.hash) {
                //Puts hash in variable, and removes the # character
                return window.location.hash.substring(1);
            }
            return "";
        }

        function HideSiblings(id) {
            $('#' + id).siblings().each(function () {
                $($(this).addClass("hidden")).hide();
            });

            $($('#' + id).removeClass("hidden")).show();
        }

        function ResetForm(id) {
            $.each($('#' + id).serializeArray(), function (i, field) {
                field.value = '';
            });
        }

        function start_auth(params) {
            var newwindow;

            var screenX = typeof window.screenX != 'undefined' ? window.screenX : window.screenLeft,
                screenY = typeof window.screenY != 'undefined' ? window.screenY : window.screenTop,
                outerWidth = typeof window.outerWidth != 'undefined' ? window.outerWidth : document.body.clientWidth,
                outerHeight = typeof window.outerHeight != 'undefined' ? window.outerHeight : (document.body.clientHeight - 22),
                width = 800,
                height = 450,
                left = parseInt(screenX + ((outerWidth - width) / 2), 10),
                top = parseInt(screenY + ((outerHeight - height) / 2.5), 10),
                features = (
                    'width=' + width +
                        ',height=' + height +
                        ',left=' + left +
                        ',top=' + top
                    );

            var start_url = params + "&return_to=<?php echo urlencode($this->current_url . "#book-step-2"); ?>" + "&date_booked=" + $('#date_booked').val() + "&time_booked=" + $('#time_booked').val() + "&people_booked=" + $('#people_booked').val();
            newwindow = window.open(start_url, 'Login', features);

            if (window.focus) {
                newwindow.focus()
            }
        }
        function AddToolTip(id, _content) {
            $(id).qtip({
                content:_content,
                position:{
                    corner:{
                        target:'topRight',
                        tooltip:'bottomLeft'
                    }
                },
                style:{
                    name:'cream',
                    tip:'bottomLeft'
                },
                show:{
                    when:{
                        event:'focus'
                    }
                },
                hide:{
                    when:{
                        event:'blur'
                    }
                }
            });
        }

        // Wait until the DOM has loaded before querying the document
        $(document).ready(function () {

            if (GetCurrentHash() != '') {
                HideSiblings(GetCurrentHash());
            }

            // Create Tabs
            $('ul.tabs').each(function () {
                var $active, $content, $links = $(this).find('a');
                $active = $links.first().addClass('active');
                $content = $($active.attr('href'));

                $links.not(':first').each(function () {
                    $($(this).attr('href')).hide();
                });

                $(this).on('click', 'a', function (e) {
                    $active.removeClass('active');
                    $content.hide();

                    $active = $(this);
                    $content = $($(this).attr('href'));

                    $active.addClass('active');
                    $content.fadeIn('fast');

                    e.preventDefault();
                    $(window).resize();
                });
            });

            // Bind Button Actions
            $("#content").on("click", "#book-now", function (event) {
                event.preventDefault();
                if ($("#booking-date-human").html() != '<?php echo $this->GetText('choose-one'); ?>') {
                    // Replicate data to hidden fields in form
                    $('#date_booked').val($('#booking-date-human').html());
                    $('#time_booked').val($('#booking-hour').val());
                    $('#people_booked').val($('#booking-qty').val());

                <?php if (!$this->Authorizer->IsLoggedIn()) { ?>
                    HideSiblings("book-step-2");
                    <?php } else { ?>
                    window.location.href = "item.php?action=book-now&user=<?php echo $_SESSION['userloggedin-username']; ?>&date_booked=" + $('#date_booked').val() + "&time_booked=" + $('#time_booked').val() + "&people_booked=" + $('#people_booked').val();
                    <?php } ?>
                }
                else {
                    alert('<?php echo $this->GetText('no-date'); ?>');
                }
            });
            $("#content").on("click", "#back-step-1", function (event) {
                HideSiblings("book-step-1");
                ResetForm('booking-form');
                $("#booking-date-human").html('<?php echo $this->GetText('choose-one'); ?>');
            });

            $("#content").on("click", "#new-reservation", function (event) {
                event.preventDefault();
                window.location.href = "index.php";
            });

            $("#content").on("click", ".ssignon", function (event) {
                event.preventDefault();
                provider = $(this).attr('provider');

                switch (provider) {
                    case "google"  :
                    case "twitter" :
                    case "facebook":
                        start_auth("index.php?provider=" + provider);
                        break;

                    default:
                        alert("Provider Error Not Found: " + provider);
                }
            });

            // Add tooltip to a particular object
            AddToolTip('input[id$=phone]', '<?php echo $this->GetText('phone-warning'); ?>');

            // Columnize divs with "wide" class.
            $('.wide').columnize({ columns:2 });
        });
    </script>
</head>
<body id="main">
<ul id="languages">
    <li id="lang-eng"><a href="?language=en" id="english" title="Switch to English">English</a></li>
    <li id="lang-spa"><a href="?language=es" id="spanish" title="Cambiar al Espa&ntilde;ol">Espa&ntilde;ol</a></li>
</ul>
<div id="content">
<div style="display:block;">
    <ul class='tabs'>
        <li><a href='#tab1'><?php echo $this->GetText('book'); ?></a></li>
        <li><a href='#tab2'><?php echo $this->GetText('info'); ?></a></li>
        <li><a href='#tab3'><?php echo $this->GetText('offer'); ?></a></li>
        <li><a href='#tab4'><?php echo $this->GetText('gallery'); ?></a></li>
        <li><a href='#tab5'><?php echo $this->GetText('menue'); ?></a></li>
        <li><a href='#tab6'><?php echo $this->GetText('recommend-us'); ?></a></li>
        <li><a href='#tab7'><?php echo $this->GetText('my-account'); ?></a></li>
    </ul>
</div>
<div class="tabbed-div round-corner-bottom shadow" id='tab1'>
    <div id="book-step-1">
        <h2><?php if (!$this->Authorizer->IsLoggedIn()) {
            echo "{$this->GetText('book-table')}";
        }
        else {
            echo $this->GetText('welcome') . " " . $this->GetUserFirstName() . ". " . $this->GetText('book-table');
        }
            ?></h2>

        <div id="calendar">
            <input class="range-low-today" id="booking-date" name="booking-date"/>
            <script type="text/javascript">
                var disabledDates = {

                };

                var lastAvailable = new Date();
                lastAvailable.setMonth(lastAvailable.getMonth() + 4);
                lastAvailable.setDate(1);
                lastAvailable.setDate(lastAvailable.getDate() - 1);

                // Create the datePicker
                datePickerController.createDatePicker({
                    // Associate the three text inputs to their date parts
                    formElements:{"booking-date":"%d-%m-%Y"},
                    // Show week numbers
                    showWeeks:false,
                    staticPos:true,
                    noTodayButton:true,
                    // Set an opacity of 90%
                    //finalOpacity:90,
                    // Remove the associated input from the U.I.
                    hideInput:true,
                    rangeLow:datePickerController.dateToYYYYMMDDStr(new Date()),
                    rangeHigh:datePickerController.dateToYYYYMMDDStr(lastAvailable),
                    callbackFunctions:{"datereturned":[disperseChanges]}
                });

                // Disable the required dates
                datePickerController.setDisabledDates("booking-date", disabledDates);

                function disperseChanges() {
                    $("#booking-date-human").html($("#booking-date").val());
                }

            </script>

        </div>
        <div id="reservation-details">
            <h3><?php echo $this->GetText('date-booked'); ?></h3>

            <p id="booking-date-human" class="input"><?php echo $this->GetText('choose-one'); ?></p>

            <h3><?php echo $this->GetText('choose'); ?></h3>
            <br/>
            <label for="booking-qty"><?php echo $this->GetText('people-booked'); ?></label>
            <select id="booking-qty" class="input">
                <?php $this->GenerateGuestForm(); ?>
            </select>
            <br/>
            <br/>
            <label for="booking-hour"><?php echo $this->GetText('time-booked'); ?></label><br/>
            <select id="booking-hour" class="input">
                <option value="1230">12:30</option>
                <option value="1300">13:00</option>
                <option value="1330">13:30</option>
                <option value="1400">14:00</option>
                <option value="1430">14:30</option>
                <option value="1500">15:00</option>
                <option value="1530">15:30</option>
                <option value="1600">16:00</option>
                <option value="1630">16:30</option>
                <option value="1700">17:00</option>
                <option value="1730">17:30</option>
                <option value="2000">20:00</option>
                <option value="2030">20:30</option>
                <option value="2100">21:00</option>
                <option value="2130">21:30</option>
                <option value="2200">22:00</option>
                <option value="2230">22:30</option>
                <option value="2300">23:00</option>
            </select>
            <br/>
            <br/>
            <a id='book-now' href='#book-now'
               class='elButtonPurple buttonGreen'><?php echo $this->GetText('book'); ?></a>
        </div>
        <div class="clear"></div>
    </div>
    <div id="book-step-2" class="hidden">
        <div class="left-column">
            <form id="booking-form" action="item.php?action=book-n-signup" class="no-border" method="post">
                <?php
                $alerts = $this->getAlerts();
                ?>
                <div>
                    <input type="text" name="date_booked" id="date_booked" class="hidden"
                           value="<?php echo $this->GetData('date_booked'); ?>"/>
                    <input type="text" name="time_booked" id="time_booked" class="hidden"
                           value="<?php echo $this->GetData('time_booked'); ?>"/>
                    <input type="text" name="people_booked" id="people_booked" class="hidden"
                           value="<?php echo $this->GetData('people_booked'); ?>"/>
                    <input type="text" name="provider_name" id="provider_name" class="hidden"
                           value="<?php echo $this->GetData('provider_name'); ?>"/>
                    <input type="text" name="provider_uid" id="provider_uid" class="hidden"
                           value="<?php echo $this->GetData('provider_uid'); ?>"/>

                    <div class="row">
                        <label for="name"><?php echo $this->GetText('name'); ?></label>
                        <input type="text" name="name" id="name" class="input"
                               value="<?php echo $this->GetData('name'); ?>"/>

                        <div class="error"><?php echo $this->getData('error_user'); ?></div>
                    </div>
                    <div class="row">
                        <label for="surname"><?php echo $this->GetText('surname'); ?></label>
                        <input type="text" name="surname" id="surname" class="input"
                               value="<?php echo $this->GetData('surname'); ?>"/>

                        <div class="error"><?php echo $this->getData('error_surname'); ?></div>
                    </div>
                    <div class="row">
                        <label for="email"><?php echo $this->GetText('email'); ?></label>
                        <input type="text" name="email" id="email" class="input"
                               value="<?php echo $this->GetData('email'); ?>"/>

                        <div class="error"><?php echo $this->getData('error_email'); ?></div>
                    </div>
                    <div class="row">
                        <label for="phone"><?php echo $this->GetText('phone'); ?></label>
                        <input type="text" name="phone" id="phone" class="input"
                               value="<?php echo $this->GetData('phone'); ?>"/>

                        <div class="error"><?php echo $this->getData('error_phone'); ?></div>
                    </div>
                    <div class="row">
                        <label for="cp"><?php echo $this->GetText('zip'); ?></label>
                        <input type="text" name="cp" id="cp" class="input" value="<?php echo $this->GetData('cp'); ?>"/>

                        <div class="error"><?php echo $this->getData('error_cp'); ?></div>
                    </div>
                    <div class="row">
                        <input type="checkbox" name="privacy" id="privacy" class=""
                               value="<?php echo $this->GetData('policy'); ?>"/>
                        <label for="privacy"><?php echo $this->GetText('policy'); ?></label>

                        <div class="error"><?php echo $this->getData('error_policy'); ?></div>
                    </div>
                    <div class="row">
                        <br/>
                        <br/>
                        <a id="back-step-1" href='#back-step-1'
                           class='elButtonPurple'><?php echo $this->GetText('change-reservation'); ?></a>
                        <input type="submit" name="submit" class="elButtonPurple buttonGreen"
                               value="<?php echo $this->GetText('book-now'); ?>"/>
                    </div>
                </div>
            </form>
        </div>
        <div class="right-column">
            <div id="user-login-service">
                <h3><?php echo $this->GetText('idp'); ?></h3>

                <div id="box-idp">
                    <a href="" provider="facebook" class="ssignon" id="facebook"></a>
                    <a href="" provider="twitter" class="ssignon" id="twitter"></a>
                    <a href="" provider="google" class="ssignon" id="google"></a>

                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
            </div>

            <div id="user-login">
                <?php echo $this->LoadLoginForm(); ?>
            </div>
        </div>
        <div class="clear"></div>
    </div>
    <div id="success" class="hidden">
        <div id="success-dialog">
            <h3 class="dialog-title"><?php echo $this->GetText('thank-you'); ?></h3> <br/>
            <img src="./views/img/qr.png" alt="QR Code" class="qr"/> <br/><br/>
            <h3 class="dialog-subtitle"><?php echo $this->GetText('invite-all'); ?></h3> <br/>
            <a href='#' class='elButtonPurple invite-email'><?php echo $this->GetText('invite-email'); ?></a>
            <a href='#' class='elButtonPurple invite-facebook'><?php echo $this->GetText('invite-facebook'); ?></a>
        </div>
        <div id="success-buttons">
            <a href='#share-facebook' class='elButtonPurple share-facebook'><?php echo $this->GetText('share'); ?></a>
            <a href='#share-twitter' class='elButtonPurple share-twitter'><?php echo $this->GetText('share'); ?></a>
            <a id="new-reservation" href='#new-reservation' class='elButtonPurple buttonGreen'><?php echo $this->GetText('new-reservation'); ?></a>
        </div>
    </div>
</div>
<div class="tabbed-div round-corner-bottom shadow" id='tab2'>
    <div class="wide clear">
        <?php echo $this->GetData('body-info'); ?>
    </div>
</div>
<div class="tabbed-div round-corner-bottom shadow" id='tab3'>
    <div class="wide clear">
        <?php echo $this->GetData('body-offer'); ?>
    </div>
</div>
<div class="tabbed-div round-corner-bottom shadow height410" id='tab4'>
    <div id="galleria">
        <?php $this->GenerateGalleryLinks() ?>
    </div>
</div>
<div class="tabbed-div round-corner-bottom shadow" id='tab5'>
    <div id="menu" class="wide clear">
        <?php echo $this->GetData('body-menue'); ?>
    </div>
</div>
<div class="tabbed-div round-corner-bottom shadow" id='tab6'>
    <h2><?php echo $this->GetText('recommend-us'); ?></h2>

    <form id="recommendation-form" action="item.php?action=recommend" method="post">
        <?php $alerts = $this->getAlerts(); ?>
        <div class="left-column">
            <div class="row">
                <label for="name"><?php echo $this->GetText('name'); ?></label>
                <input type="text" name="name" class="input" value="<?php echo $this->GetData('name'); ?>"/>

                <div class="error"><?php echo $this->getData('error_user'); ?></div>
            </div>
            <div class="row">
                <label for="email"><?php echo $this->GetText('email'); ?></label>
                <input type="text" name="email" class="input" value="<?php echo $this->GetData('email'); ?>"/>

                <div class="error"><?php echo $this->getData('error_email'); ?></div>
            </div>
            <div class="row">
                <label for="email-friend"><?php echo $this->GetText('email-friend'); ?></label>
                <textarea rows="7" name="email-friend" id="email-friend" class="input">
                </textarea>

                <div class="error"><?php echo $this->getData('error_email_friend'); ?></div>
            </div>
            <div class="row">
                <br/>
                <br/>
                <input type="submit" name="submit" class="elButtonPurple"
                       value="<?php echo $this->GetText('recommend'); ?>"/>
            </div>
        </div>
        <div class="clear"></div>
    </form>
</div>
<div class="tabbed-div round-corner-bottom shadow" id='tab7'>
    <?php if (!$this->Authorizer->IsLoggedIn()) {
    echo '<div>';
    echo $this->LoadLoginForm();
    echo '</div>';
}
else {
    ?>
    <div id='myaccount'>
        <div style="float: left;">
            <h3><?php echo $this->GetText('welcome') . " " . $this->GetUserFirstName(); ?></h3>

            <p><?php echo $this->GetText('score') ?></p>
            <br/>
            <div id='score'><?php echo $this->GetUserScore() ?></div>
        </div>

        <?php $this->RenderGiftsControl(); ?>

        <a href='logout.php?return=index.php' class='elButtonPurple' style="float:left;"><?php echo $this->GetText('log-out') ?></a>

        <div class='clear'></div>
    </div>
    <?php } ?>
</div>
<div id="footer">
    <p id="footer-left"><?php echo $this->GetText('follow-us'); ?> <a href="#" title="Enlace a Facebook">facebook</a> <a
        href="#" title="Enlace a Twitter">twitter</a></p>

    <p id="footer-right"><?php echo $this->GetText('developed-by'); ?><a id="footer-logo" href="http://shoutad.com"
                                                                         title="ShoutAD Marketing!"></a></p>

    <p id="signature" class="clear"><?php echo $this->GetText('copyright'); ?></p>
</div>
</div>
<div class="clear"></div>
</body>
</html>