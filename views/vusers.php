<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Sitio de Administraci&oacute;n</title>
        <meta http-equiv = "Content-Type" content = "text/html; charset=utf-8" />
        <link rel="stylesheet" href="views/css/style.css" type="text/css" />
        <?php $this->LoadStyleSheets(); ?>
    </head>
    <body>
    	<div id="admin-panel">
	        <div id="header">
	        	
	        </div>
	        <div id="center">
	        	<div id="menu-left">
	        		<?php $this->GetLeftMenu(); ?>
	        	</div>
	        	<div id="content">
		        	<h2><?php echo $this->GetText('users-list'); ?></h2>
		        	<div>
			        	<table id="tablelistview">
		            		<thead>
    						<tr>
    							<th scope='col'><?php echo $this->GetText('user-names'); ?></th>
    							<th scope='col'><?php echo $this->GetText('name-simple'); ?></th>
    							<th scope='col'><?php echo $this->GetText('surname-simple'); ?></th>
						    	<th scope='colgroup' colspan='2' width="30%" ><?php echo $this->GetText('actions'); ?></th>
					    	</tr>
    						</thead>
    						<tbody>
    							<?php $this->GetUsersAsTableBody(); ?>
    						</tbody>		            		
		            	</table>		            	
		            </div>
		            <br />
		            <br />
		            <br />
		            <h2><?php echo $this->GetText('users-add-update'); ?></h2>
	        		<form action="" method="post">
					<div>
						<?php $this->GetAlerts(); ?>
						<p>
						<label for="name"><?php echo $this->GetText('name-simple'); ?></label>
						<input type="text" name="name" id="name" class="input" value=""/>
						</p>
						<p>
						<label for="surname"><?php echo $this->GetText('surname-simple'); ?></label>
						<input type="text" name="surname" id="surname" class="input" value=""/>
						</p>							
						<p>
						<label for="password"><?php echo $this->GetText('password'); ?></label>
						<input type="password" name="password" id="password" class="input" value=""/>
						</p>
						
						<input type="submit" name="submit" class="button-primary submit"  value="<?php echo $this->GetText('save'); ?>"/>
						</div>
					</form>
					<p id="backtoblog"><a href="admin.php" title="<?php echo $this->GetText('go-back'); ?>"><?php echo $this->GetText('go-back'); ?></a></p>		            
	            </div>
	            <div class="clear"></div>
	        </div>
        </div>
    </body> 
</html>