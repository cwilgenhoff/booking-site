<?php

/**
 * Description of Authorizer
 *
 */

require_once(dirname(__FILE__).'/../includes/common.php');
include_once(dirname(__FILE__).'/musers.php');
include_once(dirname(__FILE__).'/../controllers/ccontroller.php');

class MAuthorizer {
    
    private static $instance;
        
    private function __construct() 
    { }
    
    private function __clone() 
    { }
    
    public static function GetInstance()  
    {  
        if (!isset(self::$instance)) { 
            self::$instance = new MAuthorizer();  
        }  
        
        return self::$instance;  
    }
    
    public static function EncodePwd($password)
    {
    	return md5($password . SALT);
    }
    
    public function ValidateAdministratorLogin($user, $password)
    {
    	$userManager = new MUsers("users");
    	return $userManager->IsValidAdministrator($user, $password);
    }
    
    public function ValidateLogin($user, $password)
    {
        $userManager = new MUsers("users");
        return $userManager->IsValidUser($user, $password);
    }    
    
    public function GetLoginNameSSignOn($provider_name, $identifier)
    {
    	$userManager = new MUsers("users");
    	return $userManager->GetLoginNameSSignOnUser($provider_name, $identifier);
    }
        
    public static function IsLoggedIn($adminrights=false)
    {
    	if ($adminrights)
    	{
	        if (!isset($_SESSION['loggedin']))
	        {
	            return FALSE;
	        }
	        
	        return $_SESSION['loggedin'];
    	}

    	if (!isset($_SESSION['userloggedin']))
    	{
    		return FALSE;
    	}
    	
    	return $_SESSION['userloggedin'];
    }
    
    public static function CheckAuthorization()
    {
    	if (MAuthorizer::IsLoggedIn(true))
	    {
    		return TRUE;            	               
	    }
        else
        {
        	PageController::Redirect("login.php");               
        }
    }
    
    public function LogOut()
    {
        session_destroy();
        session_start();
    }
}
