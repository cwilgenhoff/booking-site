<?php

include_once(dirname(__FILE__).'/mdatabase.php');
include_once(dirname(__FILE__).'/muser.php');

class MUsers {
    
    public $table;    
    
    function __construct($table) 
    {   
    	$this->table = $table;
    }
    
	/** 
    * @param integer $userID
    * @return mysqli_result MySQLi Current Result
    */ 
    public function IsUniqueUserName($username)
    {
        if (!isset($this->table) || !isset($username))
        {
            return null;
        }           

        $query = "SELECT username FROM {$this->table} WHERE username=? LIMIT 1";
        $params = array("s", $username);
        MDatabase::GetInstance()->Prepare($query, $params);
        MDatabase::GetInstance()->StoreResults();                  
        $empty = MDatabase::GetInstance()->IsEmptyResult();
		$this->Dispose();
            
        return $empty;        
    }

    /**
     * @param  string $username
     * @param  string $password
     * @return bool
     */
    public function IsValidAdministrator($username, $password)
    {
    	if (!isset($this->table) || !isset($username))
    	{
    		return null;
    	}
    
    	$query = "SELECT `username` FROM `{$this->table}` WHERE `username`=? AND `password`=? AND `mod_rights`=? LIMIT 1";
    	$params = array("ssi", $username, MAuthorizer::EncodePwd($password), 1);
		MDatabase::GetInstance()->Prepare($query, $params);
    	MDatabase::GetInstance()->StoreResults();	
    	$empty = MDatabase::GetInstance()->IsEmptyResult();
    	$this->Dispose();
    
    	return !$empty;
    }
    
	/** 
    * @param  string $username
    * @param  string $password
    * @return bool
    */ 
    public function IsValidUser($username, $password)
    {
        if (!isset($this->table) || !isset($username))
        {
            return null;
        }           

        $query = "SELECT `username` FROM `{$this->table}` WHERE `username`=? AND `password`=? AND `mod_rights`=? LIMIT 1";
        $params = array("ssi", $username, MAuthorizer::EncodePwd($password), 0);
        MDatabase::GetInstance()->Prepare($query, $params);
        MDatabase::GetInstance()->StoreResults();                  
        $empty = MDatabase::GetInstance()->IsEmptyResult();
		$this->Dispose();
            
        return !$empty;        
    }
    
    /**
     * @param string $provider_name
     * @param string $user_identifier
     * @return string username 
     */
    public function GetLoginNameSSignOnUser($provider_name, $user_identifier)
    {
    	if (!isset($this->table) || !isset($provider_name) || !isset($user_identifier))
    	{
    		return null;
    	}
    
    	$query = "SELECT `username` FROM `{$this->table}` WHERE `hybridauth_provider_name`='{$provider_name}' AND `hybridauth_provider_uid`='{$user_identifier}' AND `mod_rights`=0 LIMIT 1";
		$result = MDatabase::GetInstance()->Query($query);
    	if (is_null($result))
    	{
    		return null;
    	}
    	
    	$item = $result->fetch_object();
    	$this->Dispose();
    	return $item->username;
    }
    
    
	/**
     * 
     * @return mysqli_result MySQLi Current Result
     */
    public function GetFullRecords($orderBy)
    {
        if (!isset($this->table) || !isset($orderBy))
        {
            return null;
        }           

        $query = "SELECT * FROM `{$this->table}` ORDER BY `{$orderBy}` DESC";
        return MDatabase::GetInstance()->Query($query);        
    }
    
    /**
     * 
     * @return mysqli_result MySQLi Current Result
     */
    public function GetLightRecords($orderBy)
    {
        if (!isset($this->table) || !isset($orderBy))
        {
            return null;
        }       

        $query = "SELECT `username`, `name`, `surname` FROM `{$this->table}` WHERE `mod_rights`=1 ORDER BY `{$orderBy}` DESC";
        return MDatabase::GetInstance()->Query($query);         
    }
    
    public function GetUsernameProfile($username)
    {
        if (!isset($this->table) || !isset($username))
        {
            return null;
        }       

        $query = "SELECT `username`, `name`, `surname`, `email`, `cp`, `phone`, `score` FROM `{$this->table}` WHERE `username`='{$username}' AND `mod_rights`=0 LIMIT 1";
        $result = MDatabase::GetInstance()->Query($query);
    	if (is_null($result))
    	{
    		return null;
    	}
    	
    	$user = $result->fetch_object();
    	$this->Dispose();
    	return $user;         
    }
    
    public function DeleteRecord($username)
    {
        if (!isset($this->table) || !is_string($username))
		{
            return null;
        }

		$query = "DELETE FROM `{$this->table}` WHERE `username`=? LIMIT 1";
		$params = array("s", $username);
		return MDatabase::GetInstance()->Prepare($query, $params);
    }

    /** 
    * @param string $table
    * @param MUser $item
    */ 
    public function CreateRecord($item)
    {
        if (!isset($this->table) || !isset($item))
		{
            return null;
        }
        $query = "INSERT {$this->table} (`username`, `password`, `name`, `surname`, `email`, `cp`, `phone`, `hybridauth_provider_name`, `hybridauth_provider_uid`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
	 	$params = array("sssssisss", $item->username, MAuthorizer::EncodePwd($item->password), $item->name, $item->surname, $item->email, $item->cp, $item->phone, $item->provider_name, $item->provider_uid);
	 	$results = MDatabase::GetInstance()->Prepare($query, $params);		
	 	return $results;
    }

	/** 
    * @param string $table
    * @param MUser $item
    */ 
    public function UpdateScoreToUser($user)
    {
        if (!isset($this->table) || !isset($user))
		{
            return null;
        }
		
        $query = "UPDATE {$this->table} SET `score` = `score` + ? WHERE `username`= ?";
	 	$params = array("is", POINTS_PER_RESERVATION , $user->username);
	 	$results = MDatabase::GetInstance()->Prepare($query, $params);		
	 	return $results;
    }
	
    /** 
    * @param string $table
    * @param MUser $item
    */ 
    public function CreateAdministratorRecord($item)
    {
        if (!isset($this->table) || !isset($item))
		{
            return null;
        }
        
        $query = "INSERT {$this->table} (`username`, `password`, `name`, `surname`, `mod_rights`) VALUES (?, ?, ?, ?, ?)";
	 	$params = array("ssssi", $item->username, MAuthorizer::EncodePwd($item->password), $item->name, $item->surname, 1);
	 	$results = MDatabase::GetInstance()->Prepare($query, $params);		
	 	return $results;
    }


	public function UpdateRecord($item)
    {
    	if (!isset($this->table) || !isset($item))
		{
            return null;
        }
        
        $query = "UPDATE `{$this->table}` SET `username`=?, `password`=? WHERE `id`= ?";
		$params = array("sss", $item->username, $item->password, $item->id);
		return MDatabase::GetInstance()->Prepare($query, $params);
    }
	
	public function Dispose()
    {
    	MDatabase::GetInstance()->Dispose();
    }
}