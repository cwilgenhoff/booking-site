<?php

include_once(dirname(__FILE__) . '/mdatabase.php');
include_once(dirname(__FILE__) . '/mreservation.php');

class MReservations
{

    public $table;

    function __construct($table)
    {
        $this->table = $table;
    }

    function lock($date)
    {
        $filename = BASE_DIR_LOCKS . $date . ".lock";
        $fp = fopen($filename, "a");
        if (flock($fp, LOCK_EX | LOCK_NB)) {
            return true;
        } else {
            return false;
        }
    }

    function unlock($date)
    {
        $filename = BASE_DIR_LOCKS . $date . ".lock";
        $fp = fopen($filename, "a");
        flock($fp, LOCK_UN); // unlock the file
        unlink($filename);
    }

    /**
     *
     * @param $orderBy
     * @return mysqli_result MySQLi Current Result
     */
    public function GetFullRecords($orderBy)
    {
        if (!isset($this->table) || !isset($orderBy)) {
            return null;
        }

        $query = "SELECT * FROM {$this->table} ORDER BY {$orderBy} DESC";
        return MDatabase::GetInstance()->Query($query);
    }

    /**
     *
     * @param $orderBy
     * @return mysqli_result MySQLi Current Result
     */
    public function GetLightRecords($orderBy)
    {
        if (!isset($this->table) || !isset($orderBy)) {
            return null;
        }

        $query = "SELECT t1.id, t1.date_booked, t1.time_booked, t1.people_booked, t2.name, t2.surname, t1.date_logged FROM {$this->table} as t1 INNER JOIN users AS t2 ON t1.username = t2.username ORDER BY {$orderBy} DESC";
        return MDatabase::GetInstance()->Query($query);
    }

    /**
     * @param $itemID
     * @internal param string $table
     * @internal param \MReservation $item
     * @return mysqli_result MySQLi Current Result
     */
    public function GetRecord($itemID)
    {
        if (!isset($this->table) || !isset($itemID)) {
            return null;
        }

        $query = "SELECT `date_booked`, `time_booked`, `people_booked`, `username` FROM `{$this->table}` WHERE `id`='{$itemID}' LIMIT 1";
        $result = MDatabase::GetInstance()->Query($query);
        if (is_null($result)) {
            return null;
        }

        $item = $result->fetch_object();
        $this->Dispose();
        return $item;
    }

    /**
     * @param MReservation $item
     * @internal param string $table
     * @return mysqli_result MySQLi Current Result
     */
    private function GetCountGroupByItemDate($item)
    {
        if (!isset($this->table) || !isset($item)) {
            return null;
        }

        $query = "SELECT date_booked, time_booked FROM {$this->table} WHERE date_booked = ?;";
        $params = array("s", $item->date_booked);
        MDatabase::GetInstance()->Prepare($query, $params);
        MDatabase::GetInstance()->StoreResults();
        $result = MDatabase::GetInstance()->Count();
        MDatabase::GetInstance()->Dispose();

        return $result;
    }

    /**
     * @param MReservation $item
     * @return \mysqli_stmt|null
     * @internal param string $table
     */
    public function CreateRecord($item)
    {
        if (!isset($this->table) || !isset($item)) {
            return null;
        }

        if ($this->GetCountGroupByItemDate($item) >= 30) {
            return null;
        }

        $query = "INSERT {$this->table} (date_booked, time_booked, people_booked, username, date_logged) VALUES (?, ?, ?, ?, ?)";
        $params = array("ssiss", $item->date_booked, $item->time_booked, $item->people_booked, $item->username, $item->date_logged);
        $result = MDatabase::GetInstance()->Prepare($query, $params);

        return $result;
    }

    public function DeleteRecord($itemId)
    {
        if (!isset($this->table) || !is_numeric($itemId)) {
            return null;
        }

        $query = "DELETE FROM {$this->table} WHERE id = ? LIMIT 1";
        $params = array("s", $itemId);
        $result = MDatabase::GetInstance()->Prepare($query, $params);

        return $result;
    }

    public function UpdateRecord($item)
    {
        // TODO
    }

    public function Dispose()
    {
        MDatabase::GetInstance()->Dispose();
    }
}
