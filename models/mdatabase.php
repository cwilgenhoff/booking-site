<?php

/**
 * Description of Database
 *
 */

require_once(dirname(__FILE__).'/../includes/common.php');

class MDatabase {
    
    /**
     * 
     * @var MDatabase
     */
	private static $instance;
    
    /**
     * 
     * @var mysqli	MySQLi Database Handler
     */
    private $Database;
    
    /**
     * 
     * @var mysqli_stmt MySQLi Current Statement
     */
    private $currentStmt;
    
    /**
     * 
     * @var mysqli_result MySQLi Current Result
     */
    private $currentResult;
    
    public static $MYSQL_DATETIME_FORMAT = 'Y-m-d H:i';
    public static $MYSQL_DATE_FORMAT = 'Y-m-d';
    public static $MYSQL_TIME_FORMAT = 'H:i';
    
    private function __construct()
    {
        $this->Database = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_DB);
        $this->currentStmt = null;
		$this->SetTimeZone();		
    }
    
	private function SetTimeZone()
	{
		// Setting up TimeZone for MySQL
		$now = new DateTime();
		$mins = $now->getOffset() / 60; 
		 
		$sgn = ($mins < 0 ? -1 : 1);  
		$mins = abs($mins);  
		$hrs = floor($mins / 60);  
		$mins -= $hrs * 60; 
		
		$offset = sprintf('%+d:%02d', $hrs*$sgn, $mins); 
		$this->Database->query("SET `time_zone`='{$offset}'");
	}
	
    private function __clone() 
    { }
    
    public static function GetInstance()  
    {  
        if (!isset(self::$instance)) {  
            self::$instance = new MDatabase();  
        }  
        
        return self::$instance;   
    }
    
    public function StoreResults()
    {
    	if (isset($this->currentStmt)) 
        {
            $this->currentStmt->store_result();        	
        }
    }
    
	public function IsEmptyResult()
    {
    	if (isset($this->currentStmt)) 
        {
            return ($this->currentStmt->num_rows == 0);        	
        }
    }
    
    public function Count()
    {
    	if (isset($this->currentStmt)) 
        {
            return $this->currentStmt->num_rows;        	
        }
        
        return 0;
    }
    
	public function GetDatabase()
    {
        return $this->Database;
    }
    
    public function Dispose()
    {
        if (isset($this->currentStmt)) 
        {
            $this->currentStmt->close();        
        	$this->currentStmt = null;
        }
        
    	if (isset($this->currentResult)) 
        {
            $this->currentResult->close();        
        	$this->currentResult = null;
        }
        
        return;
    }
    
    public function Disconnect()
    {
        if (!isset($this->Database)) 
        {
            return;
        }
        
        $this->Database->close();
    }

    /**
     * 
     * @return mysqli_result MySQLi Current Result
     */
    public function Query($query)
    {
    	if (!isset($query))
    	{
    		return null;
    	}
    	
    	if ($this->currentResult = MDatabase::GetInstance()->GetDatabase()->query($query))
        {
            if ($this->currentResult->num_rows > 0)
            {    
            	return $this->currentResult;
            }            
            
            return null;
        }
        else 
        {
            die("ERROR: No se pudo ejecutar la MySQLi query.");
        }    	
    }
    
    private function refValues($arr)
    { 
    	if (strnatcmp(phpversion(),'5.3') >= 0) //Reference is required for PHP 5.3+ 
    	{ 
	        $refs = array(); 
	        foreach($arr as $key => $value) 
	            $refs[$key] = &$arr[$key]; 
	        return $refs; 
    	} 
    	return $arr; 
	}
    
    public function Prepare($query, $array_of_params)
    {
    	if (!isset($query) || !isset($array_of_params))
		{
            return null;
        }
        
        if ($this->currentStmt = MDatabase::GetInstance()->GetDatabase()->prepare($query))
        {
            call_user_func_array(array($this->currentStmt, 'bind_param'), $this->refValues($array_of_params));
            $this->currentStmt->execute();            
            
            return $this->currentStmt;
        }
        else 
        {
            die("ERROR: No se pudo preparar MySQLi Statement.");
        }    	
    }    
 }