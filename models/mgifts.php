<?php

include_once(dirname(__FILE__) . '/../models/mdatabase.php');

/**
 *
 */
class MGifts
{
    /**
     * @var string
     */
    public $table;
    /**
     * @var string
     */
    public $table_common;
    /**
     * @var string
     */
    public $table_exchanged;

    /**
     *
     */
    function __construct()
    {
        $this->table = 'gifts';
        $this->table_common = 'gifts-common';
        $this->table_exchanged = 'gifts-exchanged';
    }

    /**
     *
     * @param $id
     * @param $lang
     * @return mysqli_result|null
     */
    public function GetGift($id, $lang)
    {
        if (!isset($id) || !isset($lang)) {
            return null;
        }

        $query = "SELECT `{$this->table}`.`id`, `{$this->table_common}`.`value`, `{$this->table}`.`price`, `{$this->table}`.`imageUrl` FROM `{$this->table}` INNER JOIN `{$this->table_common}` ON `{$this->table}`.`id`=`{$this->table_common}`.`giftId` WHERE `{$this->table_common}`.`language` = '{$lang}' AND `{$this->table}`.`id` = '{$id}' LIMIT 1;";
        $result = MDatabase::GetInstance()->Query($query);
        if (is_null($result)) {
            return;
        }

        $item = $result->fetch_object();
        $this->Dispose();
        return $item->value;
    }

    /**
     * @param $lang
     * @return mysqli_result|null
     */
    public function GetGifts($lang)
    {
        if (!isset($lang)) {
            return null;
        }

        $query = "SELECT `{$this->table}`.`id`, `{$this->table_common}`.`value`, `{$this->table}`.`price`, `{$this->table}`.`imageUrl` FROM `{$this->table}` INNER JOIN `{$this->table_common}` ON `{$this->table}`.`id`=`{$this->table_common}`.`giftId` WHERE `{$this->table_common}`.`language` = '{$lang}';";
        return MDatabase::GetInstance()->Query($query);
    }

    /**
     *
     */
    public function Dispose()
    {
        MDatabase::GetInstance()->Dispose();
    }
}
