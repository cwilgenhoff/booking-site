<?php

class MReservation {
    
    public $id;
    public $date_booked;
    public $time_booked;
    public $username;
    public $people_booked;
    public $date_logged;            
    
    function __construct($id=null, $date_booked, $time_booked, $people_booked, $username) 
    {
        if (isset($id)) 
        {
            $this->id = $id;
        }
    	
        if (isset($date_booked)) 
        {
        	$date = new DateTime($date_booked);
			$this->date_booked = $date->format(MDatabase::$MYSQL_DATE_FORMAT);            
        }
        
    	if (isset($time_booked)) 
        {
            $time = new DateTime($time_booked);
			$this->time_booked = $time->format(MDatabase::$MYSQL_TIME_FORMAT);   
        }
        
    	if (isset($people_booked)) 
        {
            $this->people_booked = $people_booked;
        }
        
    	if (isset($username)) 
        {
            $this->username = $username;
        }
                
    	$now = new DateTime();
		$this->date_logged = $now->format(MDatabase::$MYSQL_DATETIME_FORMAT);;
    }
}