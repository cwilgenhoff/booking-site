<?php

include_once(dirname(__FILE__).'/../models/mdatabase.php');

class MCommons {

	public $table;

	function __construct()
	{
		$this->table = 'common';
	}

	/**
	 *
	 * @return mysqli_result MySQLi Current Result
	 */
	public function GetValue($key, $lang)
	{
		if (!isset($this->table) || !isset($key) || !isset($lang))
		{
			return null;
		}

		$query = "SELECT `value` FROM `{$this->table}` WHERE `key`='{$key}' AND `language`='{$lang}' LIMIT 1";
		$result = MDatabase::GetInstance()->Query($query);
		if (is_null($result))
		{
			return;
		}

	 $item = $result->fetch_object();
	 $this->Dispose();
	 return $item->value;
	}

	public function UpdateRecord($key, $value, $lang)
	{
		if (!isset($this->table) || !isset($key) || !isset($value) || !isset($lang))
	 {
	 	return null;
	 }

	 $query = "UPDATE {$this->table} SET `value`=? WHERE `key`=? AND `language`=?";
	 $params = array("sss", $value, $key, $lang);
	 return MDatabase::GetInstance()->Prepare($query, $params);
	}

	public function Dispose()
	{
		MDatabase::GetInstance()->Dispose();
	}
}
