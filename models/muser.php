<?php

require_once(dirname(__FILE__).'/../includes/common.php');
include_once(dirname(__FILE__).'/mauthorizer.php');

class MUser {
    
    public $username;
    public $password;
  	public $name;
  	public $surname;
  	public $email;
  	public $phone;
  	public $cp;
  	public $provider_name;
  	public $provider_uid;
        
    function __construct($name, $surname, $email, $phone, $cp, $provider_name='', $provider_uid='') 
    {
        if (isset($name)) 
        {
            $this->name = $name;
        }
        
    	if (isset($date_booked)) 
        {
            $this->time_booked = $date_booked;
        }
        
    	if (isset($time_booked)) 
        {
            $this->time_booked = $time_booked;
        }
        
	    if (isset($name)) 
        {
            $this->name= $name;
        }
	        
	    if (isset($surname)) 
        {
            $this->surname = $surname;
        }
	        
	    if (isset($email)) 
        {
            $this->email = $email;
        }
	        
	    if (isset($phone)) 
        {
            $this->phone = $phone;
        }
	        
	    if (isset($cp)) 
        {
            $this->cp = $cp;
        }

	  if (isset($provider_name)) 
        {
            $this->provider_name= $provider_name;
        }

	  if (isset($provider_uid)) 
        {
            $this->provider_uid = $provider_uid;
        }
    	
        $this->username = strtolower($name{0} . $surname);
        $this->password = $this->GeneratePassword(6,4);
    }
    
	function GeneratePassword($length=9, $strength=0) {
		$vowels = 'aeuy';
		$consonants = 'bdghjmnpqrstvz';
		if ($strength & 1) {
			$consonants .= 'BDGHJLMNPQRSTVWXZ';
		}
		if ($strength & 2) {
			$vowels .= "AEUY";
		}
		if ($strength & 4) {
			$consonants .= '23456789';
		}
		if ($strength & 8) {
			$consonants .= '@#$%';
		}
	 
		$password = '';
		$alt = time() % 2;
		for ($i = 0; $i < $length; $i++) {
			if ($alt == 1) {
				$password .= $consonants[(rand() % strlen($consonants))];
				$alt = 0;
			} else {
				$password .= $vowels[(rand() % strlen($vowels))];
				$alt = 1;
			}
		}
		return $password;
	}
}