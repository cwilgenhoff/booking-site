<?php

include_once(dirname(__FILE__).'/cadmin.php');
include_once(dirname(__FILE__).'/../models/musers.php');

class UsersManager extends Administrator {
    	
	public function __constructor() 
    {	}
	
	public function PreInit() 
    {	
    	parent::PreInit();
		$this->table = "users";    	
    }
    
    public function PerformActions()
    {
    	$userManager = new MUsers($this->table);
    	if (isset($_GET['action']))
		{
			switch ($_GET['action']) 
			{
				case 'update':
					if (!is_numeric($_GET["id"]))
					{
						return;
					}
					
					if (isset($_POST['submit']))
					{
						if ($_POST["username"] == '' || $_POST["password"] == '')
						{
							$this->SetAlert('Los campos usuario y contrase&ntilde;a son requeridos.', 'error');																			
						}
						else if (!$userManager->IsUniqueUserName($_POST["username"]))
						{
							$this->SetAlert('El nombre de usuario ingresado ya existe. Por favor, ingrese otro.', 'error');
						}
						else 
						{
							$item = $user = new MUser($_POST["name"], $_POST["surname"], '', '' , 0, '', '' );
							$userManager->UpdateRecord($item);
							$userManager->Dispose();
							$this->SetAlert('Usuario modificado con exito.', 'success');							
						}							
					}
					else
					{
						$result = $userManager->GetRecord($_GET["id"]);
						if ($post = $result->fetch_object())
						{
							$this->PushData("username", $post->username);							
						}
						
						$userManager->Dispose();
					}					
				break;
				
				case 'delete':
					if (!is_string($_GET["username"]))
					{
						return;
					}					
					
					$userManager->DeleteRecord($_GET["username"]);
					$userManager->Dispose();
					PageController::Redirect("admin.php#users");					
				break;
			}						
		}
		else 
		{
			// add
			if (isset($_POST['submit']))
			{
				if ($_POST["username"] == '' || $_POST["password"] == '')
				{
					$this->SetAlert('Los campos usuario y contrase&ntilde;a son requeridos.', 'error');																			
				}
				else if (!$userManager->IsUniqueUserName($_POST["username"]))
				{
					$this->SetAlert('El nombre de usuario ingresado ya existe. Por favor, ingrese otro.', 'error');
				}
				else
				{
					$item = new MUser(null, $_POST["username"], $_POST["password"]);
					$userManager->CreateRecord($item);
					$userManager->Dispose();
					$this->SetAlert('Usuario agregado con exito.', 'success');					
				}					
			}
		}		
    }
    
	public function GetUsersAsTableBody()
    {
		$userManager = new MUsers($this->table);
    	$result = $userManager->GetLightRecords("username");
    	if (is_null($result))
    	{
    		return;
    	}
    	
    	while ($post = $result->fetch_object())
		{
			echo "<tr>";
			echo "<td>" . $post->username . "</td>";
			echo "<td>" . $post->name . "</td>";
			echo "<td>" . $post->surname . "</td>";
			echo "<td><a title='' href='users.php?action=update&amp;username=" . $post->username . "' class='edit'>" . $this->GetText('edit') . "</a></td>";
			echo "<td><a title='' href='users.php?action=delete&amp;username=" . $post->username . "' class='delete'>" . $this->GetText('delete') . "</a></td>";
			echo "</tr>";						
		}	
		
		$userManager->Dispose();
    }
}