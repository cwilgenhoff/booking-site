<?php

include_once(dirname(__FILE__) . '/ccontroller.php');
include_once(dirname(__FILE__) . '/../models/mcommons.php');
include_once(dirname(__FILE__) . '/../models/mgifts.php');
require_once(dirname(__FILE__) . '/../includes/hybridauth/hybridauth/Hybrid/Auth.php');
include_once(dirname(__FILE__) . '/../includes/hybridauth/hybridauth/Hybrid/User_Profile.php');

class Index extends PageController
{

    public function __constructor()
    {
    }

    public function PreInit()
    {
        parent::PreInit();
        $this->SetAuthorizer(MAuthorizer::getInstance());
        $this->FillTabs();

        if (isset($_POST['submit'])) {
            $this->PushData('username', $_POST['username']);
            $this->PushData('password', $_POST['password']);
        }
        else if (isset($_SESSION['ssignon_data']) && !isset($_GET["provider"]) && !isset($_GET["return_to"])) {
            // store data
            $user = $_SESSION['ssignon_data']['user'];

            $this->PushData('date_booked', $_SESSION['ssignon_data']['reservation']['date_booked']);
            $this->PushData('time_booked', $_SESSION['ssignon_data']['reservation']['time_booked']);
            $this->PushData('people_booked', $_SESSION['ssignon_data']['reservation']['people_booked']);
            $this->PushData('provider_name', $_SESSION['ssignon_data']['provider_name']);
            $this->PushData('provider_uid', $user->identifier);
            $this->PushData('return_to', $_SESSION['ssignon_data']['return_to']);
            $this->PushData('name', $user->firstName);
            $this->PushData('surname', $user->lastName);
            $this->PushData('email', $user->email);
            $this->PushData('cp', $user->zip);
            $this->PushData('phone', $user->phone);

            $this->SetAlert($this->GetText('warning-fill-all'), 'warning');
        }
    }

    public function Init($file)
    {
        if (!$this->Authorizer->IsLoggedIn()) {
            if (isset($_GET["provider"]) && isset($_GET["return_to"])) //se va a loggear o crear user con su cuenta de FB/TW/OpenID automaticamente
            {
                $provider_name = $_GET["provider"];
                // the new user has choosen FB/TW/OpenID
                try {
                    $config = dirname(__FILE__) . '/../includes/hybridauth/hybridauth/config.php';
                    // initialize Hybrid_Auth with a given file
                    $hybridauth = new Hybrid_Auth($config);

                    // try to authenticate with the selected provider
                    $adapter = $hybridauth->authenticate($provider_name);

                    // then grab the user profile
                    $user_profile = $adapter->getUserProfile();

                    //store user data on session, after the reservation please delete reservation data.
                    $reservation = array('date_booked' => $_GET['date_booked'], 'time_booked' => $_GET['time_booked'], 'people_booked' => $_GET['people_booked']);
                    $_SESSION['ssignon_data'] = array('provider_name' => $provider_name, 'user' => $user_profile, 'return_to' => $_GET["return_to"], 'reservation' => $reservation);

                    if ($hybridauth->isConnectedWith($provider_name)) {
                        $username = $this->Authorizer->GetLoginNameSSignOn($provider_name, $user_profile->identifier);

                        if (isset($username)) {
                            //Si el usuario existe hace la reserva
                            $_SESSION['userloggedin-username'] = $username;
                            $_SESSION['userloggedin'] = TRUE;
                            $return_to = $this->base_url . "/reservarestaurante/item.php?action=book-now&user=" . $_SESSION['userloggedin-username'] . "&date_booked=" . $_GET['date_booked'] . "&time_booked=" . $_GET['time_booked'] . "&people_booked=" . $_GET['people_booked'];
                            $return_to = "http://shoutad.com/reservarestaurante/#book-now";
                            error_log($return_to);
                            parent::Redirect("index.php?provider=$provider_name&return_to=" . urlencode($return_to));
                        }
                    }
                }
                catch (Exception $e) {
                    print_r($e->getMessage());
                    $this->SetAlert($this->GetText('error-try-again') . $e->getMessage(), 'error');
                }
            }
            else if (isset($_POST["submit"])) //se va a loggear con su cuenta manualmente
            {
                if ($this->Authorizer->ValidateLogin($this->data['username'], $this->data['password']) == FALSE) {
                    // invalid login
                    $this->SetAlert($this->GetText('error-invalid-account'), 'error');
                    parent::Redirect("index.php#failure");
                }
                else {
                    // successful log in
                    $_SESSION['userloggedin-username'] = $this->data['username'];
                    $_SESSION['userloggedin'] = TRUE;
                    parent::Redirect("item.php?action=book-now&user=" . $_SESSION['username'] . "&date_booked=" . $_GET['date_booked'] . "&time_booked=" . $_GET['time_booked'] . "&people_booked=" . $_GET['people_booked']);
                    return;
                }
            }
        }
        parent::Init($file);
    }

    public function DrawLoadingMessage()
    {
        $html = "</head>
			<body>
				<img src='./views/img/loading.gif' alt='loading..' />
			</body>
			</html>";

        $html = str_replace("\t", '', $html);
        $html = str_replace("&", '&amp;', $html);
        echo $html;
    }

    public function getReturnUrl()
    {
        try {

            if (!isset($_GET["provider"]) || !isset($_GET["return_to"])) {
                return;
            }

            $return_to = $_GET['return_to'];
            $provider = $_GET['provider'];

            $config = dirname(__FILE__) . '/../includes/hybridauth/hybridauth/config.php';
            // initialize Hybrid_Auth with a given file
            $hybridauth = new Hybrid_Auth($config);

            if (!empty($provider) && $hybridauth->isConnectedWith($provider)) {
                $script = "<script type=\"text/javascript\">
					if (window.location.hash == '#_=_') window.location.href = window.location.href.replace(/#_=_/, '');
					function Redirect(url) {
					  \"use strict\";
					  setTimeout(function() {
					  	window.opener.document.location = url;						
						window.opener.document.location.reload(true);
						window.self.close();
					  }, 1000);
					}
					if(window.opener) {							
						Redirect(\"{$return_to}\");								         				                
					}
					</script>
					</head>
					<body>						
					</body>
					</html>";
                $script = str_replace("\t", '', $script);
                $script = str_replace("&", '&amp;', $script);
                echo $script;
                die();
            }
        }
        catch (Exception $e) {
            print_r($e->getMessage());
            $this->SetAlert($this->GetText('error-try-again') . $e->getMessage(), 'error');
        }

        return '';
    }

    public function LoadLoginForm()
    {
        $form = "<h3>{$this->GetText('login-title')}</h3>
				<form id='booking-form' action='index.php' method='post'>
					<p>{$this->GetText('login-subtitle')}</p>
					<div class='row'>
						<label for='username'>{$this->GetText('username')}</label>
						<input type='text' name='username' class='input' value='{$this->GetData('username')}' />
						<div class='error'>{$this->getData('error_username')}</div>
					</div>
					<div class='row'>
						<label for='password'>{$this->GetText('password')}</label>
						<input type='text' name='password' class='input' value='{$this->GetData('password')}' />
						<div class='error'>{$this->getData('error_pass')}</div>
					</div>
					<div class='row'>
						<input type='checkbox' name='remember-me' id='remember-me' class='' value='' />
						<label for='remember-me'>{$this->GetText('remember-me')}</label>																		
					</div>
					<div class='row'>
						<a href='#' class='orange' title='{$this->GetText('forgot-password')}'>{$this->GetText('forgot-password')}</a>
						<br />									
						<br />
					</div>	
					<div class='row'>
						<input type='submit' name='submit' class='elButtonPurple buttonGreenLogin' value='{$this->GetText('sign-in')}' />									
					</div>																
				</form>
				<div class='clear'></div>";

        return $form;
    }

    public function GetUserFirstName()
    {
        if (!$this->Authorizer->IsLoggedIn()) {
            return '';
        }

        $userManager = new MUsers("users");
        $user = $userManager->GetUsernameProfile($_SESSION['userloggedin-username']);

        return $user->name;
    }

    public function GetUserScore()
    {
        if (!$this->Authorizer->IsLoggedIn()) {
            return '';
        }

        $userManager = new MUsers("users");
        $user = $userManager->GetUsernameProfile($_SESSION['userloggedin-username']);

        return $user->score;
    }

    public function RenderGiftsControl()
    {
        if (!$this->Authorizer->IsLoggedIn()) {
            return '';
        }

        $giftManager = new MGifts("users");
        $result = $giftManager->GetGifts($_SESSION['language']);
        if (is_null($result)) {
            return;
        }

        echo "<div id='gifts'>";
        echo "<p>{$this->GetText('exchange')}</p>";
        while ($gift = $result->fetch_object()) {
            echo "  <div class='gift'>";
            echo "      <p>{$gift->value}</p>";
            echo "      <span>({$gift->price} pts)</span>";
            echo "      <a href='item.php?action=exchange&id={$gift->id}' title='{$gift->value}'>";
            echo "          <img alt='{$gift->value}' src='{$gift->imageUrl}'/>";
            echo "      </a>";
            echo "  </div>";
        }
        echo "<div class='clear'></div>";
        echo "</div>";

        $giftManager->Dispose();
    }

    public function LoadStyleSheets()
    {

    }

    private function FillTabs()
    {
        $commons = new MCommons();

        $this->PushHtmlData('body-info', $commons->GetValue('body-info', $_SESSION['language']));
        $this->PushHtmlData('body-menue', $commons->GetValue('body-menue', $_SESSION['language']));
        $this->PushHtmlData('body-offer', $commons->GetValue('body-offer', $_SESSION['language']));
        $this->PushData('name-restaurant', $commons->GetValue('name-restaurant', $_SESSION['language']));
    }

    public function GetDisabledDays()
    {
        return 'disable-date-25-03-2012';
    }

    public function GenerateGuestForm()
    {
        for ($counter = 1; $counter < 11; $counter++) {
            $people = $counter;
            if ($counter == 1) {
                $people = $counter . " " . $this->GetText('person');
            }
            else {
                $people = $counter . " " . $this->GetText('people');
            }
            echo "<option value=" . $counter . ">" . $people . "</option>";
        }

    }

    public function GenerateHoursForm()
    {

    }

    public function GenerateGalleryLinks()
    {
        $dir = dirname(__FILE__) . '/../gallery/slideshow/';
        $files = scandir($dir);

        foreach ($files as $key => $value) {
            if ($value != '.' && $value != '..') {
                echo "<a href='./gallery/slideshow/{$value}'><img data-title='' data-description='' src='./gallery/.thumbs/slideshow/{$value}' /></a>";
            }
        }
    }
}
					