<?php

include_once(dirname(__FILE__) . '/ccontroller.php');
include_once(dirname(__FILE__) . '/../models/mreservations.php');
include_once(dirname(__FILE__) . '/../models/mcommons.php');
include_once(dirname(__FILE__) . '/../includes/utils.php');

class ItemManager extends PageController
{

    /*
    * @var string
    */
    protected $table_users;

    private $utilities;

    public function __constructor()
    {
    }

    public function Init($file)
    {
        $this->PerformActions();
    }

    public function PreInit()
    {
        if (isset($_GET['action'])) {
            error_log("Accessing CItem.php. Action [GET]: " . $_GET['action']);
        }
        if (isset($_POST['action'])) {
            error_log("Accessing CItem.php. Action [POST]: " . $_POST['action']);
        }
        parent::PreInit();

        $this->table = "reservations";
        $this->table_users = "users";
        $this->utilities = new Utils();
    }

    private function CreateReservation($reservation, $itemManager)
    {
        $result = $itemManager->CreateRecord($reservation);

        if ($result == null) {
            PageController::Redirect("index.php#failure");
            $itemManager->Dispose();
            return;
        }

        $reservation->id = $result->insert_id;
        $itemManager->Dispose();

        $this->utilities->UrlRequest($this->base_url . "/reservarestaurante/item.php", array('action' => 'notificate', 'id' => "$reservation->id"));

        // Set Session Variables for Recently registered user
        $_SESSION['userloggedin-username'] = $reservation->username;
        $_SESSION['userloggedin'] = TRUE;

        PageController::Redirect("index.php#success");
    }

    public function PerformActions()
    {
        if (isset($_GET['action'])) {
            $action = $_GET['action'];
        }
        else if (isset($_POST['action'])) {
            $action = $_POST['action'];
        }

        if (isset($action)) {
            $itemManager = new MReservations($this->table);
            $userManager = new MUsers($this->table_users);

            error_log("Entering Action: " . $action);

            switch ($action) {
                case 'book-n-signup':
                    if (isset($_POST['submit'])) {
                        if ($_POST["date_booked"] == '' || $_POST["time_booked"] == '' || $_POST["people_booked"] == '' || $_POST["email"] == '' || $_POST["phone"] == '' || $_POST["name"] == '' || $_POST["surname"] == '') {
                            $this->SetAlert($this->GetText('warning-fill-all'), 'error');
                        }
                        else {
                            $user = new MUser($_POST["name"], $_POST["surname"], $_POST["email"], $_POST["phone"], $_POST["cp"], $_POST["provider_name"], $_POST["provider_uid"]);

                            if ($userManager->IsUniqueUserName($user->username)) {
                                $userManager->CreateRecord($user);
                                $userManager->Dispose();
                            }


                            $reservation = new MReservation(null, $_POST["date_booked"], $_POST["time_booked"], $_POST["people_booked"], $user->username);
                            $this->CreateReservation($reservation, $itemManager);
                        }
                    }
                    break;
                case 'book-now':
                    if ($_GET["user"] == '' || $_GET["date_booked"] == '' || $_GET["time_booked"] == '' || $_GET["people_booked"] == '') {
                        $this->SetAlert($this->GetText('warning-fill-all'), 'error');
                    }
                    else {
                        $user = $userManager->GetUsernameProfile($_GET["user"]);

                        if (!isset($user)) {
                            $this->SetAlert($this->GetText('error-invalid-account'), 'error');
                            PageController::Redirect("index.php#failure");
                        }
                        else {
                            $reservation = new MReservation(null, $_GET["date_booked"], $_GET["time_booked"], $_GET["people_booked"], $_GET["user"]);
                            $this->CreateReservation($reservation, $itemManager);
                        }
                    }
                    break;

                case 'notificate':
                    error_log("Notification Block: Started!");
                    if (!is_numeric($_POST["id"])) {
                        return;
                    }

                    if ($_POST["id"] == '') {
                        $this->SetAlert($this->GetText('warning-fill-all'), 'error');
                    }
                    else {
                        $item = $itemManager->GetRecord($_POST["id"]);

                        if (isset($item)) {
                            $reservation = new MReservation($item->id, $item->date_booked, $item->time_booked, $item->people_booked, $item->username);
                            $user = $userManager->GetUsernameProfile($item->username);
                            if (isset($reservation) && isset($user)) {
                                $userManager->UpdateScoreToUser($user);
                                $this->SendEmailToUser($user, $reservation);
                                $this->SendEmailToRestaurant($user, $reservation);
                                $this->SendSMSToUser($user, $reservation);
                                $this->AddSubscriberToCampaingMonitor($user);
                            }
                        }
                    }
                    error_log("Notification Block: Ended!");
                    break;

                case 'delete':
                    if (!is_numeric($_GET["id"])) {
                        return;
                    }

                    $itemManager->DeleteRecord($_GET["id"]);
                    $itemManager->Dispose();
                    PageController::Redirect("admin.php");
                    break;

                case 'recommend':
                    if (isset($_POST['submit'])) {
                        if (!isset($_POST['name']) || !isset($_POST['email']) || !isset($_POST['email-friend'])) {
                            return;
                        }

                        $this->SendRecommendations($_POST['name'], MAIN_EMAIL_ADDRESS, $_POST['email-friend']);
                    }

                    PageController::Redirect("index.php");
                    break;
            }
        }
        else {
            PageController::Redirect("index.php");
        }
    }

    private function ProcessMacros($user, $item, $text)
    {
        $time_resv = new DateTime($item->time_booked);
        $date_resv = strftime($this->GetText('date-format'), strtotime($item->date_booked));

        $macros_keys = array("[time]", "[date]", "[people]", "[name]", "[surname]", "[phone]", "[zip]", "[email]");
        $macros_data = array($time_resv->format(MDatabase::$MYSQL_TIME_FORMAT),
            $date_resv,
            $item->people_booked,
            $user->name,
            $user->surname,
            $user->phone,
            $user->cp,
            $user->email);

        $text = str_replace($macros_keys, $macros_data, $text);
        return $text;
    }

    public function SendEmailToUser($user, $item)
    {
        if (!isset($user) || !isset($item)) {
            return;
        }

        $commons = new MCommons();
        $body = $commons->GetValue('email-client-confirmation', $_SESSION['language']);

        if (!isset($body)) {
            return;
        }

        $body = $this->ProcessMacros($user, $item, $body);

        if (!$this->utilities->SendEmail(MAIN_EMAIL_ADDRESS, $user, $this->GetText('thank-you'), $body)) {
            $this->SetAlert($this->GetText('error-email-failed'), 'error');
        }
    }

    function SendEmailToRestaurant($user, $item)
    {
        if (!isset($user) || !isset($item)) {
            return;
        }

        $commons = new MCommons();
        $body = $commons->GetValue('email-restaurant-confirmation', $_SESSION['language']);

        if (!isset($body)) {
            return;
        }

        $body = $this->ProcessMacros($user, $item, $body);
        $subject = $this->ProcessMacros($user, $item, $this->GetText('new-reservation-made'));

        if (!$this->utilities->SendEmailManually(MAIN_EMAIL_ADDRESS, MAIN_EMAIL_ADDRESS, $subject, $body)) {
            $this->SetAlert($this->GetText('error-email-failed'), 'error');
        }
    }

    public function SendSMSToUser($user, $item)
    {
        if (!isset($user) || !isset($item)) {
            return;
        }

        $commons = new MCommons();
        $body = $commons->GetValue('sms-confirmation', $_SESSION['language']);

        if (!isset($body)) {
            return;
        }

        $from = 'ShoutAD';
        $body = $this->ProcessMacros($user, $item, $body);
        $this->utilities->SendSMS($from, $user, $body);
    }


    public function SendRecommendations($name, $email, $friends)
    {
        // We assume $friends variables has emails separated by commas
        $friends = explode(',', str_replace(' ', '', trim($friends)));

        // Commons Data
        $commons = new MCommons();

        // Email Content
        $fullname = explode(' ', trim($name));
        $subject = str_replace("[name]", $fullname[0], $this->GetText('recommends-you'));
        $body = str_replace("[name]", $fullname[0], $commons->GetValue('email-recommendations', $_SESSION['language']));

        foreach ($friends as $friend_email) {
            if ($this->utilities->ValidateEmail($friend_email)) {
                print_r(array($email, $friend_email, $subject, $body));
                if ($this->utilities->SendEmailManually(MAIN_EMAIL_ADDRESS, $friend_email, $subject, $body)) {
                    // Adding user to campaing monitor list
                    $this->utilities->AddSubscriberToCampagingMonitor($email, '', '');
                }
                else {
                    $this->SetAlert($this->GetText('error-email-failed'), 'error');
                }
            }
            else {
                echo "FAILED: " . $friend_email . " <br />";
            }
        }

        return true;
    }

    public function AddSubscriberToCampaingMonitor($user)
    {
        if (!isset($user)) {
            return;
        }

        return $this->utilities->AddSubscriberToCampagingMonitor($user->email, $user->name, $user->surname);
    }

}
	