<?php

include_once(dirname(__FILE__).'/ccontroller.php');

class Login extends PageController {
    
    public function __constructor() 
    { }
    
    public function PreInit() 
    {   
        if (isset($_POST['submit']))
        {
            $this->PushData('username', $_POST['username']);
            $this->PushData('password', $_POST['password']);            
        }
        
        $this->SetAuthorizer(MAuthorizer::getInstance());
        parent::PreInit();
    }
    
    public function Init($file) 
    {
        if (isset($this->Authorizer))
        {
            if (isset($_POST['submit']))
            {
            	if ($_POST['username'] == '' || $_POST['password'] == '')
				{
					$this->SetAlert('Por favor, llenar todos los campos.', 'error');					
				}
                else if ($this->Authorizer->ValidateAdministratorLogin($this->data['username'], $this->data['password']) == FALSE)
                {
                    // invalid login
					$this->SetAlert('Usuario o Contrase&ntilde;a invalido!', 'error');					
                }
                else
                {
                	// successful log in
                	$_SESSION['username'] = $this->GetData('username');
                    $_SESSION['loggedin'] = TRUE;
                }
            }
            
            // If current user is already logged-in we redirect to admin page. 
            if ($this->Authorizer->IsLoggedIn(true))
            {
                $this->Redirect("admin.php");
            }
            else
            {
                parent::Init($file);
            }
        }        
    }
}
