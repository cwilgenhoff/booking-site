<?php

/**
 * Controller Base Class
 * 
 */

include_once(dirname(__FILE__).'/../models/mauthorizer.php');
include_once(dirname(__FILE__).'/../models/mdatabase.php');
include_once(dirname(__FILE__).'/../includes/languages.php');

abstract class PageController {
    
	/*
    * @var array
    */
	protected $data;
	
	/*
    * @var array
    */
	protected $alertTypes;
	
	/*
    * @var MAuthorizer
    */
    protected $Authorizer;
    
    /*
    * @var string
    */
    protected $table;
    
    protected $current_url;

    protected $base_url;

    /*
    * Default constructors
    */
    public function __constructor()
    { }

    /*
    * Members
    */
    public function SetAuthorizer($auth)
    {
        $this->Authorizer = $auth;
    }
    
	private function CleanMagicQuotes()
	{
		if (get_magic_quotes_gpc()) {
			$process = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
			while (list($key, $val) = each($process)) {
				foreach ($val as $k => $v) {
					unset($process[$key][$k]);
					if (is_array($v)) {
						$process[$key][stripslashes($k)] = $v;
						$process[] = &$process[$key][stripslashes($k)];
					} else {
						$process[$key][stripslashes($k)] = stripslashes($v);
					}
				}
			}
			unset($process);
		}
	}
	
    /*
     * Aim: By default a setup authorizer is needed
     */
    public function PreInit() 
    { 
        session_start();
		
		$this->CleanMagicQuotes();       
		
        if (!isset($_SESSION['language']) && !isset($_GET['language']))
        {
        	$_SESSION['language'] = 'es';
        }
        else if (isset($_GET['language']))
        {
        	if (Languages::IsValidLanguage($_GET['language']))
        	{
        		$_SESSION['language'] = $_GET['language']; 
        	}
        }
		
		if ($_SESSION['language'] == 'es')
		{
			setlocale(LC_ALL,"es_ES@euro","es_ES","esp");
		}
		else if ($_SESSION['language'] == 'en')
		{
			setlocale(LC_ALL, 'en', 'GB', 'en-gb', 'en_US');
		}
		
		$this->SetAlertTypes(array('success', 'warning', 'error'));
                $this->base_url = (!empty($_SERVER['HTTPS'])) ? "https://".$_SERVER['SERVER_NAME'] : "http://".$_SERVER['SERVER_NAME'];
		$this->current_url = $this->base_url . $_SERVER['REQUEST_URI'];

    }

    public function Init($file) 
    {
        include($file);
    }

    public function PostInit()
    {
    	unset($this->data); 
    }
     
    public function Load($file) 
    {
        $this->PreInit();
        $this->Init($file);
        $this->PostInit();
    }
    
    public function PushData($key, $value)
    {
        $this->data[$key] = htmlentities($value, ENT_QUOTES);
    }
    
    public function PushHtmlData($key, $value)
    {
        $this->data[$key] = $value;
    }

    public function GetData($key)
    {
    	if (isset($this->data[$key]))
		{
			return $this->data[$key];
		}
		else
		{
			return '';
		}
    }
    
    public static function Redirect($url)
    {
        header("Location: $url");
    }

    public function LoadStyleSheets()
    {
    	
    }
    
    public function GetText($key)
    {
    	//according to $_SESSION['language'] is going to retrieve a text
    	
    	 return Languages::Get($_SESSION['language'], $key);
    }
    
	public function GetCurrentLanguage()
    {
    	return $_SESSION['language'];
    }
    
	/*
		Get / Set Alerts
	*/
	function SetAlertTypes($types)
	{
		$this->alertTypes = $types;
	}
	
	function SetAlert($value, $type = null)
	{
		if ($type == '') 
		{ 
			$type = $this->alertTypes[0]; 
		}
		$_SESSION[$type][] = $value;
	}
	
	function GetAlerts()
	{
		$data = '';
		foreach($this->alertTypes as $alert)
		{			
			if (isset($_SESSION[$alert]))
			{
				foreach($_SESSION[$alert] as $value)
				{
					$data .= '<li class="'. $alert .'">' . $value . '</li>';
				}
				unset($_SESSION[$alert]);
			}
		}
		
		if ($data != '') 
		{ 
			echo '<ul class="alerts">' . $data . '</ul>'; 
		}		
	}
}