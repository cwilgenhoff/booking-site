<?php

include_once(dirname(__FILE__) . '/ccontroller.php');
include_once(dirname(__FILE__) . '/../models/mreservations.php');
include_once(dirname(__FILE__) . '/../models/mcommons.php');

class Administrator extends PageController
{

    public function __constructor()
    {
    }

    public function PreInit()
    {
        parent::PreInit();
        $this->SetAuthorizer(MAuthorizer::getInstance());
        $this->table = "reservations";
    }

    public function Init($file)
    {
        if (isset($this->Authorizer)) {
            if ($this->Authorizer->CheckAuthorization()) {
                $this->PerformActions();
                parent::Init($file);
            }
        }
    }

    private function FillForms()
    {
        $itemManager = new MCommons();

        $this->PushData('body-info', $itemManager->GetValue('body-info', $this->GetCurrentLanguage()));
        $this->PushData('body-menue', $itemManager->GetValue('body-menue', $this->GetCurrentLanguage()));
        $this->PushData('body-offer', $itemManager->GetValue('body-offer', $this->GetCurrentLanguage()));
        $this->PushData('name-restaurant', $itemManager->GetValue('name-restaurant', $this->GetCurrentLanguage()));
        $this->PushData('max-reservation-restaurant', $itemManager->GetValue('max-reservation-restaurant', $this->GetCurrentLanguage()));
        $this->PushData('sms-confirmation', $itemManager->GetValue('sms-confirmation', $this->GetCurrentLanguage()));
        $this->PushData('email-client-confirmation', $itemManager->GetValue('email-client-confirmation', $this->GetCurrentLanguage()));
        $this->PushData('email-restaurant-confirmation', $itemManager->GetValue('email-restaurant-confirmation', $this->GetCurrentLanguage()));
        $this->PushData('email-recommendations', $itemManager->GetValue('email-recommendations', $this->GetCurrentLanguage()));
    }

    public function PerformActions()
    {
        if (isset($_GET['action'])) {
            $itemManager = new MCommons();
            switch ($_GET['action']) {
                case 'info':
                    if (isset($_POST['body-info'])) {
                        $itemManager->UpdateRecord('body-info', $_POST['body-info'], $this->GetCurrentLanguage());
                        $itemManager->Dispose();
                        PageController::Redirect("admin.php#info");
                    }
                    else {
                        $this->SetAlert('El campo contenido es requerido.', 'error');
                    }
                    break;
                case 'menue':
                    if (isset($_POST['body-menue'])) {
                        $itemManager->UpdateRecord('body-menue', $_POST['body-menue'], $this->GetCurrentLanguage());
                        $itemManager->Dispose();
                        PageController::Redirect("admin.php#menue");
                    }
                    else {
                        $this->SetAlert('El campo contenido es requerido.', 'error');
                    }
                    break;
                case 'offer':
                    if (isset($_POST['body-offer'])) {
                        $itemManager->UpdateRecord('body-offer', $_POST['body-offer'], $this->GetCurrentLanguage());
                        $itemManager->Dispose();
                        PageController::Redirect("admin.php#offer");
                    }
                    else {
                        $this->SetAlert('El campo URL del restaurant es requerido.', 'error');
                    }
                    break;
                case 'configs':
                    if (isset($_POST['name-restaurant'])) {
                        $itemManager->UpdateRecord('name-restaurant', $_POST['name-restaurant'], $this->GetCurrentLanguage());
                        $itemManager->Dispose();
                    }

                    if (isset($_POST['max-reservation-restaurant'])) {
                        $itemManager->UpdateRecord('max-reservation-restaurant', $_POST['max-reservation-restaurant'], $this->GetCurrentLanguage());
                        $itemManager->Dispose();
                    }

                    if (isset($_POST['sms-confirmation'])) {
                        $itemManager->UpdateRecord('sms-confirmation', $_POST['sms-confirmation'], $this->GetCurrentLanguage());
                        $itemManager->Dispose();
                    }

                    if (isset($_POST['email-client-confirmation'])) {
                        $itemManager->UpdateRecord('email-client-confirmation', $_POST['email-client-confirmation'], $this->GetCurrentLanguage());
                        $itemManager->Dispose();
                    }

                    if (isset($_POST['email-restaurant-confirmation'])) {
                        $itemManager->UpdateRecord('email-restaurant-confirmation', $_POST['email-restaurant-confirmation'], $this->GetCurrentLanguage());
                        $itemManager->Dispose();
                    }

                    if (isset($_POST['email-recommendations'])) {
                        $itemManager->UpdateRecord('email-recommendations', $_POST['email-recommendations'], $this->GetCurrentLanguage());
                        $itemManager->Dispose();
                    }

                    PageController::Redirect("admin.php#configs");
                    break;
            }
        }
        else {
            $this->FillForms();
        }
    }

    public function GetReservationsAsTableBody()
    {
        // Item manager for reservations
        $itemManager = new MReservations($this->table);
        $result = $itemManager->GetLightRecords("date_booked, time_booked");
        if (is_null($result)) {
            return;
        }

        while ($reservation = $result->fetch_object()) {
            $date_logged = strftime($this->GetText('date-format'), strtotime($reservation->date_logged));
            echo "<tr>";
            echo "<td scope='row'>" . $reservation->date_booked . "</td>";
            echo "<td scope='row'>" . $reservation->time_booked . "</td>";
            echo "<td scope='row'>" . $reservation->people_booked . "</td>";
            echo "<td scope='row'>" . $reservation->name . "</td>";
            echo "<td scope='row'>" . $reservation->surname . "</td>";
            echo "<td scope='row'>" . $date_logged . "</td>";
            echo "<td><a title='' href='item.php?action=delete&amp;id=" . $reservation->id . "' class='delete'>" . $this->GetText('delete') . "</a></td>";
            echo "</tr>";
        }

        $itemManager->Dispose();
    }

    public function GetLeftMenu()
    {
        echo "<a href='admin.php#reservations' title='Administrar reservas' id='reservations' class='menu-item'>" . $this->GetText('reservations') . "</a>";
        echo "<a href='admin.php#offer' title='Administrar Ofertas' id='offer' class='menu-item'>" . $this->GetText('offer') . "</a>";
        echo "<a href='admin.php#info' title='Administrar informaci&oacute;n y ofertas' id='info' class='menu-item'>" . $this->GetText('info') . "</a>";
        echo "<a href='admin.php#gallery' title='Administrar galleria' id='gallery' class='menu-item'>" . $this->GetText('gallery') . "</a>";
        echo "<a href='admin.php#menue' title='Administrar carta' id='menue' class='menu-item'>" . $this->GetText('menue') . "</a>";
        echo "<a href='admin.php#configs' title='Administrar notificaciones' id='configs' class='menu-item'>" . $this->GetText('configurations') . "</a>";
        echo "<a href='admin.php#users' title='Administrar usuarios' id='users' class='menu-item'>" . $this->GetText('users') . "</a>";
        echo "<a href='logout.php?return=admin.php' title='" . $this->GetText('log-out') . "' class='menu-item'>" . $this->GetText('log-out') . "</a>";
    }
}