<?php
include_once(dirname(__FILE__) . '/PHPMailer/class.phpmailer.php');
include_once(dirname(__FILE__) . '/sms.php');
include_once(dirname(__FILE__) . '/cm-wrapper/csrest_subscribers.php');
include_once(dirname(__FILE__) . '/common.php');

class Utils
{
    private $mail;
    private $cm;
    private $sms;

    public function __constructor()
    {
    }

    private function SetupEmail()
    {
        $this->mail = new PHPMailer();
        $this->mail->IsSMTP(); // telling the class to use SMTP
        $this->mail->SMTPDebug = 0; // enables SMTP debug information (for testing)
        // 1 = errors and messages
        // 2 = messages only

        $this->mail->SMTPAuth = SMTP_AUTH; // enable SMTP authentication

        if (SMTP_AUTH) {
            $this->mail->SMTPSecure = SMTP_SECURE; // sets the prefix to the servier
        }

        $this->mail->Host = SMTP_HOST;
        $this->mail->Port = SMTP_PORT;
        $this->mail->Username = SMTP_USERNAME;
        $this->mail->Password = SMTP_PASSWORD;
    }

    public function SendEmail($from, $user, $subject, $body)
    {
        $this->SetupEmail();

        $this->mail->SetFrom($from, 'ShoutAD - Reserva Mesa');

        $this->mail->AddReplyTo('no-reply@shoutad.com', 'ShoutAD - Reserva Mesa');

        $this->mail->Subject = $subject;

        $this->mail->MsgHTML($body);

        $this->mail->AddAddress($user->email, $user->name . " " . $user->surname);

        return $this->mail->Send();
    }

    public function SendEmailManually($from, $to, $subject, $body)
    {

        $this->SetupEmail();

        $this->mail->SetFrom($from, 'ShoutAD - Reserva Mesa');

        $this->mail->AddReplyTo('no-reply@shoutad.com', 'ShoutAD - Reserva Mesa');

        $this->mail->Subject = $subject;

        $this->mail->MsgHTML($body);

        $this->mail->AddAddress($to, 'ShoutAD - Reserva Mesa');

        return $this->mail->Send();
    }

    private function SetupCampaingMonitorAPI()
    {
        $this->cm = new CS_REST_Subscribers(CAMPAING_MON_LIST_ID, CAMPAING_MON_API_ID);
    }

    public function AddSubscriberToCampagingMonitor($email, $name, $surname)
    {
        $this->SetupCampaingMonitorAPI();

        //This is the actual call to the method, passing email address, name.
        $result = $this->cm->add(array(
                'EmailAddress' => $email,
                'Name' => $name . ' ' . $surname,
                'Resubscribe' => true)
        );

        if ($result->was_successful()) {
            return true;
        }
        else {
            return false;
        }
    }

    private function SetupSMS()
    {
        $this->sms = new sms(); // Create SMS object.
        $this->sms->username = SMS_GATEWAY_USERNAME; // The HTTP API username of your account.
        $this->sms->password = SMS_GATEWAY_PASSWORD; // The HTTP API password of your account.
    }

    public function SendSMS($from, $user, $body)
    {
        $this->SetupSMS();

        $this->sms->msgtext = $body; // The SMS Message text.
        $this->sms->originator = $from; // The desired Originator of your message.
        $this->sms->phone = $user->phone; // The full International mobile number of the

        $result = $this->sms->send(); // Call method send() to send SMS Message.
    }

    public function ValidateEmail($email)
    {
        if (!preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $email)) {
            return false;
        }
        return true;
    }

    public function UrlRequest($url, $params)
    {
        foreach ($params as $key => &$val) {
            if (is_array($val)) $val = implode(',', $val);
            $post_params[] = $key . '=' . urlencode($val);
        }

        $post_string = implode('&', $post_params);

        $parts = parse_url($url);

        $fp = fsockopen($parts['host'],
            isset($parts['port']) ? $parts['port'] : 80,
            $errno, $errstr, 1);

        if (!$fp) {
            error_log("Couldn't open a socket to " . $url . " (" . $errstr . "). Code: " . $errno);
            return;
        }

        $out = "POST " . $parts['path'] . " HTTP/1.1\r\n";
        $out .= "Host: " . $parts['host'] . "\r\n";
        $out .= "Content-Type: application/x-www-form-urlencoded\r\n";
        $out .= "Content-Length: " . strlen($post_string) . "\r\n";
        $out .= "Connection: Close\r\n\r\n";

        if (isset($post_string)) $out .= $post_string;
        error_log($out);

        fwrite($fp, $out);
        fclose($fp);
    }
}