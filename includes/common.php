<?php

/**
 * Common Configurations
 * 
 */

// database configuration
define ( 'DB_HOST', 'localhost' );  
define ( 'DB_USER', 'root' );  
define ( 'DB_PASSWORD', 'toor' );  
define ( 'DB_DB', 'booking-db' );  

// user authentication
define ( 'SALT', 'tye0(x3' );

// TIMEZONE
define ( 'TIMEZONE_DEFAULT', 'Europe/Madrid' );
date_default_timezone_set(TIMEZONE_DEFAULT); 

define ( 'BASE_DIR_LOCKS', './locks/');

// smtp configuration
define ( 'MAIN_EMAIL_ADDRESS', 'reservas.shoutad@gmail.com');
define ( 'SMTP_AUTH', true);
define ( 'SMTP_SECURE', 'ssl');
define ( 'SMTP_HOST', 'smtp.gmail.com');
define ( 'SMTP_PORT', 465);
define ( 'SMTP_USERNAME', 'reservas.shoutad@gmail.com');
define ( 'SMTP_PASSWORD', 'paulrand3');

// sms gateway configuration
define ( 'SMS_GATEWAY_USERNAME', 'gonzalo@shoutad.com');
define ( 'SMS_GATEWAY_PASSWORD', 'B90A6520');

// campaing monitor configuration
define ( 'CAMPAING_MON_API_ID', 'e0371f86d73aa9496791cf0c55231fe7');
define ( 'CAMPAING_MON_LIST_ID', 'd5794eb55200a54af8ad2696ed45bb2f');

define ( 'POINTS_PER_RESERVATION', '50');

// error reporting
mysqli_report(MYSQLI_REPORT_ERROR);