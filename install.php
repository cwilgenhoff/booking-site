<?php

include(dirname(__FILE__).'/models/mdatabase.php');

class Installer {
	
	public function __constructor()
	{ }
	
	public function Install()
	{
		$es_common="CREATE TABLE IF NOT EXISTS `es_common` ( `id` int(11) NOT NULL AUTO_INCREMENT, `title` varchar(32) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL, `body` text CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ; ";
		$en_common="CREATE TABLE IF NOT EXISTS `en_common` ( `id` int(11) NOT NULL AUTO_INCREMENT, `title` varchar(32) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL, `body` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ; ";								
		$reservations ="CREATE TABLE IF NOT EXISTS `reservations` (`id` INT(11) NOT NULL AUTO_INCREMENT, `date_booked` date NOT NULL, `time_booked` time NOT NULL, `people_booked` INT(11) NOT NULL, `username` varchar(32) NOT NULL, `date_logged` datetime NOT NULL, PRIMARY KEY(`id`), INDEX (`username`), FOREIGN KEY (`username`) REFERENCES users(`username`)) ENGINE=INNODB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";								
		$users = "CREATE TABLE IF NOT EXISTS `users` (`username` varchar(32) NOT NULL, `password` varchar(32) NOT NULL, `name` varchar(32) NOT NULL, `surname` varchar(32) NOT NULL, `email` varchar(32) NOT NULL, `cp` int(8) NOT NULL, `phone` varchar(32) NOT NULL, PRIMARY KEY (`username`) ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";		
		$admin = "INSERT INTO `users` (`username`, `password`, `name`, `surname`, `email`, `cp`, `phone`) VALUES ('admin', '1ce6b41c3ae97eae0e86cc9a7707a252', 'Administrator', '', 'info@marbellaresto.com', '', '');";
		
		
		echo "<h1>Installer:</h1>";
		echo "<p>Selected Database: " . DB_DB . "</p>";
		echo "<p>Creating `es_common` table!</p>";
		MDatabase::GetInstance()->GetDatabase()->query($es_common);
		MDatabase::GetInstance()->Dispose();
		echo "<p>Creating `en_common` table!</p>";
		MDatabase::GetInstance()->GetDatabase()->query($en_common);
		MDatabase::GetInstance()->Dispose();
		echo "<p>Creating `users` table!</p>";
		MDatabase::GetInstance()->GetDatabase()->query($users);
		MDatabase::GetInstance()->Dispose();
		echo "<p>Creating `reservations` table!</p>";
		MDatabase::GetInstance()->GetDatabase()->query($reservations);
		MDatabase::GetInstance()->Dispose();
		echo "<p>Inserting into users -> user: `admin` password: `admin`!</p>";
		MDatabase::GetInstance()->GetDatabase()->query($admin);
		MDatabase::GetInstance()->Dispose();
		echo "<p><strong>Sucessfully Installed!</strong></p>";
	}
	
	public function Uninstall()
	{
		$tables = "DROP TABLE IF EXISTS reservations; DROP TABLE IF EXISTS users; DROP TABLE IF EXISTS es_common; DROP TABLE IF EXISTS en_common;";
		MDatabase::GetInstance()->GetDatabase()->query($tables);
		MDatabase::GetInstance()->Dispose();
	}
}

$Installer = new Installer();
$Installer->Install();